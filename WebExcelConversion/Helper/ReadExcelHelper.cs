﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace WebExcelConversion.Helper
{
    public static class ReadExcelHelper
    {
        private enum Formats
        {
            General = 0,
            Number = 1,
            Decimal = 2,
            Currency = 164,
            Accounting = 44,
            DateShort = 14,
            DateLong = 165,
            Time = 166,
            Percentage = 10,
            Fraction = 12,
            Scientific = 11,
            Text = 49
        }
        public static string GetColumnName(string cellReference)
        {
            var regex = new Regex("[A-Za-z]+");
            var match = regex.Match(cellReference);

            return match.Value;
        }

        public static int ConvertColumnNameToNumber(string columnName)
        {
            var alpha = new Regex("^[A-Z]+$");
            if (!alpha.IsMatch(columnName)) throw new ArgumentException();

            char[] colLetters = columnName.ToCharArray();
            Array.Reverse(colLetters);

            var convertedValue = 0;
            for (int i = 0; i < colLetters.Length; i++)
            {
                char letter = colLetters[i];
                int current = i == 0 ? letter - 65 : letter - 64; // ASCII 'A' = 65
                convertedValue += current * (int)Math.Pow(26, i);
            }

            return convertedValue;
        }

        public static IEnumerator<Cell> GetExcelCellEnumerator(Row row)
        {
            int currentCount = 0;
            foreach (Cell cell in row.Descendants<Cell>())
            {
                string columnName = GetColumnName(cell.CellReference);

                int currentColumnIndex = ConvertColumnNameToNumber(columnName);

                for (; currentCount < currentColumnIndex; currentCount++)
                {
                    var emptycell = new Cell() { DataType = null, CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(string.Empty) };
                    yield return emptycell;
                }

                yield return cell;
                currentCount++;
            }
        }

        public static string ReadExcelCell(Cell cell, WorkbookPart workbookPart)
        {
            var cellValue = cell.CellValue;
            var text = (cellValue == null) ? cell.InnerText : cellValue.Text;

            if ((cell.DataType != null) && (cell.DataType == CellValues.SharedString))
            {
                text = workbookPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>().ElementAt(
                        Convert.ToInt32(cell.CellValue.Text)).InnerText;
            }
            else
            {
                if (cell.DataType == null) // number & dates
                {

                 
                    if (cell.StyleIndex != null)
                    {
                        int styleIndex = (int)cell.StyleIndex.Value;
                        CellFormat cellFormat = (CellFormat)workbookPart.WorkbookStylesPart.Stylesheet.CellFormats.ElementAt(styleIndex);
                        uint formatId = cellFormat.NumberFormatId.Value;

                        //if (formatId == (uint)Formats.DateShort || formatId == (uint)Formats.DateLong)
                        //{
                        //    double oaDate;
                        //    if (double.TryParse(cell.InnerText, out oaDate))
                        //    {
                        //        text = DateTime.FromOADate(oaDate).ToShortDateString();
                        //    }
                        //}
                        if (formatId == 0 || formatId == 20)
                        {
                            return text;
                        }
                        else if ((formatId == 165 && text.Split('.').Count() == 3) || formatId == 166) //kadang ada decimal dengan formatId 165, jadi ada validasi text.spli('.')
                        {
                            double oaDate;
                            if (double.TryParse(cell.InnerText, out oaDate))
                            {
                                
                                //di format disini, sebelumnya format ada helper GetCustomDate kalau pakai itu format jadi MM.dd.yyyy
                                text = DateTime.FromOADate(oaDate).ToString("dd.MM.yyyy");
                                //text = DateTime.FromOADate(oaDate).ToShortDateString();
                            }
                        }

                        //else
                        //{
                        //    var dd2 = Convert.ToDouble(cell.InnerText.Replace(".", ","));
                        //    TimeSpan t = DateTime.FromOADate(dd2).TimeOfDay;
                        //    text = t.ToString(@"hh\:mm\:ss");
                        //}
                    }


                }
            }

            double doubtext;

            return (text == null ? string.Empty : double.TryParse(text, out doubtext) ? Convert.ToDecimal(doubtext).ToString() : text).Replace("'", "''");
        }
    }
}