﻿using Microsoft.AspNet.SignalR;

namespace WebExcelConversion.Helper
{
    public class ConversionHub : Hub
    {
        public void Hello()
        {
            Clients.All.hello();
        }

        public void GetData()
        {
            int generatedValue = 10;
            var data = new
            {
                generatedValue = generatedValue
            };
            Clients.Caller.setData(data);
        }

    }
}