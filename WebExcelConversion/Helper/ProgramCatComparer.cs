﻿using System;
using System.Collections.Generic;
using WebExcelConversion.Models;

namespace WebExcelConversion.Helper
{
    public class ProgramCatComparer : IEqualityComparer<UploadTemplate>
    {
        public bool Equals(UploadTemplate x, UploadTemplate y)
        {
            if (string.Equals(x.ProgramCat, y.ProgramCat, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }
            return false;
        }

        public int GetHashCode(UploadTemplate obj) => obj.OutletNumber.GetHashCode();
    }
}