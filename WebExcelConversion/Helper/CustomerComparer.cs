﻿using System;
using System.Collections.Generic;
using WebExcelConversion.Models;

namespace WebExcelConversion.Helper
{
    public class CustomerComparer : IEqualityComparer<UploadTemplate>
    {
        public bool Equals(UploadTemplate x, UploadTemplate y)
        {
            if (string.Equals(x.OutletNumber, y.OutletNumber, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }
            return false;
        }

        public int GetHashCode(UploadTemplate obj) => obj.OutletNumber.GetHashCode();
    }
}