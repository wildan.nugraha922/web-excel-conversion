﻿using System;

namespace WebExcelConversion.Helper
{
    public static class ConvertHelper
    {
        public static string GetCustomDate(string date)
        {
            try
            {
                double oaDate;
                string result = "";
                if (double.TryParse(date, out oaDate))
                    result = DateTime.FromOADate(oaDate).ToString("dd.MM.yyyy");
                    return result;
               
                //dicomment karena kadang return nya jadi MM.dd.yyyy
                //var temp = Convert.ToDateTime(date);
                //string result = temp.ToString("dd.MM.yyy");
                //return result;
                //return date;
            }
            catch (Exception ex)
            {

                return date;
            }
        }

        
        public static string GetCustomDecimal(string valueDecimal, int panjangAngka) {
            try
            {
                var mathRound = Math.Round(Convert.ToDecimal(valueDecimal), panjangAngka);
                return Convert.ToString(mathRound);
            }
            catch (Exception ex)
            {
                return valueDecimal;
            }
        }

       
        public static string GetRoundDecimal(string valueDecimal) {
            try
            {
                var mathRound = Math.Round(decimal.Parse(valueDecimal));
                return mathRound.ToString().Replace(",", "");
            }
            catch (Exception ex)
            {
                return valueDecimal;
            }
           
        }

    }
}