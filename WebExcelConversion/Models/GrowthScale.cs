﻿namespace WebExcelConversion.Models
{
    public class GrowthScale
    {
        public string GrowthPercentage { get; set; }
        public string ScaleCurrency { get; set; }
        public string ScaleQuantity { get; set; }
        public string ScaleUnitOfMeasure { get; set; }
        public string ConditionCurrency { get; set; }
        public string ConditionRate { get; set; }
        public string OutletNumber { get; set; }
        public string TradingChain { get; set; }
    }
}