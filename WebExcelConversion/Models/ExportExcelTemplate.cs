﻿using System.Collections.Generic;
using WebExcelConversion.Enum;

namespace WebExcelConversion.Models
{

    public class ExportExcelWrapper
    {
        public Trade Trade { get; set; }
        public TradeType TradeType { get; set; }
        public ExportExcelTemplateGT ExcelTemplateForGT { get; set; }
        public ExportExcelTemplateMTTT ExcelTemplateForMTTT { get; set; }
        public ExportExcelTemplateMTTP ExcelTemplateForMTTP { get; set; }
        public ExportExcelTemplateMTTPAuto ExcelTemplateForMTTPAuto { get; set; }
        public bool IsSucced { get; set; }
        public List<string> ErrorLog { get; set; }
        public int ResultCount { get; set; }
    }

    public class ExportExcelTemplateGT
    {
        public List<HeaderTemplate> ListHeaderTemplate { get; set; }
        public List<ProgramCategoryTemplate> ListProgramCategoryTemplate { get; set; }
        public List<RulesTemplate> ListRulesOfProgramCategory { get; set; }
        public List<RulesTemplate> ListRulesOfEligibleSoldTo { get; set; }
        public List<RulesTemplate> ListRulesOfSellOut { get; set; }
    }
    public class ExportExcelTemplateMTTT
    {
        public List<HeaderTemplate> ListHeaderTemplate { get; set; }
        public List<ProgramCategoryTemplate> ListProgramCategoryTemplate { get; set; }
        public List<RulesMTTemplate> ListRulesOfProgramCategory { get; set; }
        public List<RulesMTTemplate> ListRulesOfEligibleTradingChain { get; set; }
        public List<RulesMTTemplate> ListRulesOfGrowthAllProduct { get; set; }
        public List<RulesMTTemplate> ListRulesOfGrowthEstmAccrual { get; set; }
        public List<RulesMTTemplate> ListRulesOfGrowthCalculation { get; set; }
        public List<RulesMTTemplate> ListRulesOfGrowthTier { get; set; }
        public List<GrowthScale> ListRulesOfGrowthScale { get; set; }
    }
    public class ExportExcelTemplateMTTP
    {
        public List<HeaderTemplate> ListHeaderTemplate { get; set; }
        public List<ProgramCategoryTemplate> ListProgramCategoryTemplate { get; set; }
        public List<RulesMTTemplate> ListRulesOfProgramCategory { get; set; }
        public List<RulesMTTemplate> ListRulesOfEligibleChainOrOperationalRegion { get; set; }
    }
    public class ExportExcelTemplateMTTPAuto
    {
        public List<HeaderTemplate> ListHeaderTemplate { get; set; }
        public List<ProgramCategoryTemplate> ListProgramCategoryTemplate { get; set; }
        public List<RulesMTTemplate> ListRulesOfProgramCategory { get; set; }
        public List<RulesMTTemplate> ListRulesOfEligibilityOperationalRegion { get; set; }
        public List<RulesMTTemplate> ListRulesOfEligibilityTradingChain { get; set; }
    }
}