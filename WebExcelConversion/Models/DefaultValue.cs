﻿namespace WebExcelConversion.Models
{
    public class DefaultValue
    {
        //DB path 

        public string DbPath { get; set; }
        #region HeaderTemplate
        //Max Character
        public int MaxLengthDescription { get; set; }
        public int MaxLengthType { get; set; }
        public int MaxLengthPaymentTerms { get; set; }
        // Default Value
        public string MasterRequestType { get; set; }
        public string CurrencyH { get; set; }
        public string SalesOrganization { get; set; }
        public string DistributionChannel { get; set; }
        public string Division { get; set; }
        public string ExternalDescription { get; set; }
        public string SpendAmountCurrency { get; set; }
        public string SpendAmountCurrencyMTTP { get; set; }
        public string ContactPerson { get; set; }

        #endregion


        #region Program Categorry
        public string SettlementFactor { get; set; }
        public string SettlementFrequency { get; set; }
        public string PaymentLevel { get; set; }
        public string CurrencyPC { get; set; }
        #endregion


        #region Rules Of Program Category
        public string ConditionCurrencyPC { get; set; }
        public string ConditionTypeEmpty { get; set; }
        public string ConditionTypeNotEmpty { get; set; }
        public string Table { get; set; }
        //exclude program cat for MT - TT
        public string ExcludeProgramCat { get; set; }

        #endregion


        #region Rules Of Eligible Sold To
        public string ConditionTypeEST { get; set; }
        public string TableEST { get; set; }
        #endregion

        #region Rules Of Sell Out
        public string ConditionTypeSO { get; set; }
        public string TableSO { get; set; }
        public string PromotionId { get; set; }
        public string ConditionCurrency { get; set; }
        public string Per { get; set; }
        public string UnitOfMeasure { get; set; }
        #endregion

        public DefaultValue()
        {
            DbPath = @"~/MyData.db";

            #region Header
            MaxLengthDescription = 40;
            MaxLengthType = 2;
            MaxLengthPaymentTerms = 4;
            MasterRequestType = "Z001";
            CurrencyH = "IDR";
            SalesOrganization = "ID02";
            DistributionChannel = "Z1";
            Division = "Z0";
            ExternalDescription = "";
            SpendAmountCurrency = "";
            SpendAmountCurrencyMTTP = "IDR";
            ContactPerson = "";
            #endregion

            #region Program Categorry
            SettlementFactor = "SI";
            SettlementFrequency = "ZCYEAR";
            PaymentLevel = "PH";
            CurrencyPC = "IDR";
            #endregion

            #region Rules Of Program Category
            ConditionTypeEmpty = "ZV07";
            ConditionTypeNotEmpty = "ZV11";
            ConditionCurrencyPC = "%";
            Table = "751";
            ExcludeProgramCat = "Z016";
            #endregion

            #region Rules Of Eligible Sold To
            ConditionTypeEST = "ZV03";
            TableEST = "750";
            #endregion

            #region Rules Of Sell Out
            ConditionTypeSO = "ZV17";
            TableSO = "751";
            PromotionId = "Z002";
            ConditionCurrency = "IDR";
            Per = "1";
            UnitOfMeasure = "CS";
            #endregion
        }
    }
}