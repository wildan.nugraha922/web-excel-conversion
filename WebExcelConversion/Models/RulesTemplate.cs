﻿namespace WebExcelConversion.Models
{

    public class RulesTemplate
    {
        public string ConditionType { get; set; }
        public string Table { get; set; }
        public string TradeName { get; set; }
        public string OperationalRegion { get; set; }
        public string SoldToParty { get; set; }
        public string MaterialGroup1 { get; set; }
        public string MaterialGroup2 { get; set; }
        public string MaterialGroup3 { get; set; }
        public string PromotionId { get; set; }
        public string SkpNumber { get; set; }
        public string SkpAmount { get; set; }
        public string ConditionCurrency { get; set; }
        public string Rate { get; set; }
        public string Per { get; set; }
        public string UnitOfMeasure { get; set; }
        public string ValidFrom { get; set; }
        public string ValidTo { get; set; }
        public string AccrueDollar { get; set; }
        public string AccDollarUOM { get; set; }
        public string RebateBasis { get; set; }
        public string PayoutBasis { get; set; }
        public string PYSalValue { get; set; }
        public string PYAgreement { get; set; }
        public string RefCurrency { get; set; }

        //nav property
        public string OutletNumber { get; set; }
        public string TradingChain { get; set; }
    }

}