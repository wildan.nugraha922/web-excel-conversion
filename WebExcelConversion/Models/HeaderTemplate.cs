﻿namespace WebExcelConversion.Models
{
    public class HeaderTemplate
    {
        public string MasterRequestType { get; set; }
        public string Description { get; set; }
        public string Currency { get; set; }
        public string ValidFrom { get; set; }
        public string ValidTo { get; set; }
        public string SalesOrganization { get; set; }
        public string DistributionChannel { get; set; }
        public string Division { get; set; }
        public string ContractValidFrom { get; set; }
        public string ContractValidTo { get; set; }
        public string MarketChannel { get; set; }
        public string Type { get; set; }
        public string ExternalDescription { get; set; }
        public string SpendAmmountCurrency { get; set; }
        public string PaymentTerms { get; set; }
        public string Payer { get; set; }
        public string ContactPerson { get; set; }
        public string GMID { get; set; }

        //navigation property for MTTP
        public int Index { get; set; }


        //template ini hanya digunakan diCustomer MT -TT dengan type  Create Auto Over and Above
        public string StatusFlowApprover { get; set; }
        public string TradingChain { get; set; }


    }
}