﻿namespace WebExcelConversion.Models
{
    public class GroupByMTTT
    {
        public string OutletNumber { get; set; }
        public string TradingChain { get; set; }
    }
}