﻿namespace WebExcelConversion.Models
{
    public class UploadTemplate
    {
        public int No { get; set; }
        public string OutletNumber { get; set; }
        public string CustGroupName { get; set; }
        public string LegalContractYes { get; set; }
        public string LegalContractNo { get; set; }
        public string MTGT { get; set; }
        public string Type { get; set; }
        public string TradingChain { get; set; }
        public string EligibleOperationalRegion { get; set; }
        public string PPA { get; set; }
        public string ProgramCategory { get; set; }
        public string ProgramCat { get; set; }
        public string BudgetPerProgramCat { get; set; }
        public string PeriodStart { get; set; }
        public string PeriodEnd { get; set; }
        public string RatePercentage { get; set; }
        public string RateIDR { get; set; }
        public string MaterialGroup1Desc { get; set; }
        public string MaterialGroup1 { get; set; }
        public string PreviousYearSalesValue { get; set; }
        public string GrowthScal1GrowthPercentage { get; set; }
        public string GrowthScal1RatePercentage { get; set; }
        public string GrowthScal2GrowthPercentage { get; set; }
        public string GrowthScal2RatePercentage { get; set; }
        public string GrowthScal3GrowthPercentage { get; set; }
        public string GrowthScal3RatePercentage { get; set; }
        public string PaymentTerms { get; set; }
        public string Settlement { get; set; }
        public string GMID { get; set; }
        public string SellOutRatePercentage { get; set; }
        public string SellOutRateIDR { get; set; }
    }
}