﻿using System;

namespace WebExcelConversion.Models
{
    public class Conversion
    {
        public string Id { get; set; }
        public int TotalHeader { get; set; }
        public string Value { get; set; }
        public DateTime CreatedDate { get; set; }
    }

}