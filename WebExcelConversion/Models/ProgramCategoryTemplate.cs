﻿namespace WebExcelConversion.Models
{
    public class ProgramCategoryTemplate
    {
        public string PromotionID { get; set; }
        public string SettlementFactor { get; set; }
        public string SettlementMethod { get; set; }
        public string SpendAmount { get; set; }
        public string SettlementFrequency { get; set; }
        public string PaymentLevel { get; set; }
        public string RebateReceipnt { get; set; }
        public string ValidFrom { get; set; }
        public string ValidTo { get; set; }
        public string ProgramCategory { get; set; }
        public string Currency { get; set; }
        //navigation for MTTP
        public int Index { get; set; }
        public string TradingChain { get; set; }
    }
}