﻿namespace WebExcelConversion.Models
{
    public class RulesMTTemplate : RulesTemplate
    {
        public string DisDollarUOM { get; set; }
        public int Index { get; set; }
        public string PriceSheetDescription { get; set; }
        public string PromotionID { get; set; }

    }
}