﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using LiteDB;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebExcelConversion.Enum;
using WebExcelConversion.Helper;
using WebExcelConversion.Models;

namespace WebExcelConversion.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.ActionAgreement = true;
            ViewBag.IsUploaded = false;
            //if (TempData["IsSucced"] != null)
            //{
            //    var isSucced = (bool)TempData["IsSucced"];
            //    if (isSucced)
            //    {
            //        ExportExcelWrapper data = (ExportExcelWrapper)TempData["ExcelData"];
            //        return RedirectToAction("DownloadFile", new { excelTemplate = data });
            //    }
            //}

            if (TempData["IsFailed"] != null)
            {
                if ((bool)TempData["IsFailed"] == true)
                {
                    ViewBag.IsFailed = true;
                    ViewBag.ErrorLog = TempData["ErrorLog"];
                }
                else
                {
                    ViewBag.IsFailed = false;
                    ViewBag.ErrorLog = "";
                }
            }

            return View();
        }

        //public ActionResult Test()
        //{
        //    var listError = new List<string>();
        //    listError.Add("Error 1");
        //    listError.Add("Error 2");
        //    listError.Add("Error 3");

        //    ViewBag.ErrorLog = listError;
        //    ViewBag.IsFailed = true;
        //    return View("Index");
        //}

        [HttpPost]
        public ActionResult DownloadFile()
        {
            string id = Request.Form["hideGuidName"];
            TempData["IsSucced"] = false;
            DeleteOldData();
            var conversion = GetData(id);
            if (conversion != null)
            {

                var excelTemplate = JsonConvert.DeserializeObject<ExportExcelWrapper>(conversion);
                var stream = GenerateFile(excelTemplate);
                ViewBag.Message = "File uploaded successfully";
                ViewBag.IsSucced = true;
                ViewBag.IsUploaded = true;
                TempData["TotalHeader"] = excelTemplate.ResultCount;
                string excelFileName = "";
                if (excelTemplate.Trade == Trade.GT)
                {
                    excelFileName = "Result_Customer_GT.xlsx";
                }
                else
                {
                    if (excelTemplate.TradeType == TradeType.TT)
                    {
                        excelFileName = "Result_Customer_MT_TT.xlsx";
                    }
                    else
                    {
                        excelFileName = "Result_Customer_MT_TP.xlsx";
                    }
                }
                ViewBag.Message = "File has been generated";

                DeleteData(id);

                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelFileName);
            }
            else
            {
                var listError = new List<string>() { "File has been deleted, please reupload file" };
                TempData["IsFailed"] = true;
                TempData["ErrorLog"] = listError;
                return RedirectToAction("Index");
            }
        }

        public ActionResult Upload()
        {
            TempData["IsSucced"] = null;
            TempData["IsFailed"] = null;
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file)

        {
            var agreementType = Request.Form["agreementType"];

            if (agreementType == "agreement")
            {
                ViewBag.ActionAgreement = true;
            }
            else
            {
                ViewBag.ActionOverAndAbove = true;
            }

            ViewBag.IsSucced = false;
            if (file != null && file.ContentLength > 0)
                try
                {
                    var excelTemplate = ReadExcel(file, agreementType);

                    if (!excelTemplate.IsSucced)
                    {
                        ViewBag.Message = "Something wrong";
                        ViewBag.ErrorLog = excelTemplate.ErrorLog;
                        ViewBag.IsFailed = true;
                        TempData["IsSucced"] = false;
                        ViewBag.IsUploaded = false;

                    }
                    else
                    {
                        string jsonString = JsonConvert.SerializeObject(excelTemplate);
                        string guidString = Guid.NewGuid().ToString();
                        var conversion = new Conversion
                        {
                            Id = guidString,
                            TotalHeader = excelTemplate.ResultCount,
                            Value = jsonString,
                            CreatedDate = DateTime.Now
                        };
                        AddData(conversion);
                        ViewBag.IsUploaded = true;
                        TempData["ExcelData"] = excelTemplate;
                        TempData["IsSucced"] = true;
                        TempData["ConversionGuid"] = guidString;
                        TempData["TotalHeader"] = excelTemplate.ResultCount;
                        TempData["IsFailed"] = null;
                    }

                    return View("Index");

                    //var stream = GenerateFile(excelTemplate);
                    //ViewBag.Message = "File uploaded successfully";
                    //ViewBag.IsSucced = true;
                    //ViewBag.IsUploaded = true;
                    //string excelFileName = "";
                    //if (excelTemplate.Trade == Trade.GT)
                    //{
                    //    excelFileName = "Result_Customer_GT.xlsx";
                    //}
                    //else
                    //{
                    //    if (excelTemplate.TradeType == TradeType.TT)
                    //    {
                    //        excelFileName = "Result_Customer_MT_TT.xlsx";
                    //    }
                    //    else
                    //    {
                    //        excelFileName = "Result_Customer_MT_TP.xlsx";
                    //    }
                    //}
                    //ViewBag.Message = "File has been generated";
                    //return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelFileName);
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }
            return View("Index");
        }

        private Cell ConstructCell(string value, CellValues dataType)
        {
            return new Cell()
            {
                CellValue = new CellValue(value),
                DataType = new EnumValue<CellValues>(dataType)
            };
        }

        public MemoryStream GenerateFile(ExportExcelWrapper data)
        {

            var stream = new MemoryStream();
            Trade trade = data.Trade;
            TradeType tradeType = data.TradeType;
            bool isMTTP = trade == Trade.MT && tradeType == TradeType.TP ? true : false;

            #region Customer GT
            var listHeader = data.ExcelTemplateForGT.ListHeaderTemplate;
            var listProgramCategory = data.ExcelTemplateForGT.ListProgramCategoryTemplate;
            var listRulesOfProgramCategory = data.ExcelTemplateForGT.ListRulesOfProgramCategory;
            var listRulesOfEligibleSoldTo = data.ExcelTemplateForGT.ListRulesOfEligibleSoldTo;
            var listRulesOfSellOut = data.ExcelTemplateForGT.ListRulesOfSellOut;
            #endregion

            #region Customer MT-TT
            var listHeaderMTTT = data.ExcelTemplateForMTTT.ListHeaderTemplate;
            var listProgramCategoryMTTT = data.ExcelTemplateForMTTT.ListProgramCategoryTemplate;
            var listRulesOfProgramCategoryMTTT = data.ExcelTemplateForMTTT.ListRulesOfProgramCategory;
            var listRulesOfEligibleTradingChain = data.ExcelTemplateForMTTT.ListRulesOfEligibleTradingChain;
            var listRulesOfGrowthAllProduct = data.ExcelTemplateForMTTT.ListRulesOfGrowthAllProduct;
            var listRulesOfGrowthEstmAccrual = data.ExcelTemplateForMTTT.ListRulesOfGrowthEstmAccrual;
            var listRulesOfGrowthCalculation = data.ExcelTemplateForMTTT.ListRulesOfGrowthCalculation;
            var listRulesOfGrowthTier = data.ExcelTemplateForMTTT.ListRulesOfGrowthTier;
            var listRulesOfGrowthScale = data.ExcelTemplateForMTTT.ListRulesOfGrowthScale;
            #endregion

            #region Customer MT-TP
            var listHeaderMTTP = data.ExcelTemplateForMTTP.ListHeaderTemplate;
            var listProgramCategoryMTTP = data.ExcelTemplateForMTTP.ListProgramCategoryTemplate;
            var listRulesOfProgramCategoryMTTP = data.ExcelTemplateForMTTP.ListRulesOfProgramCategory;
            var listRulesOfEligibleChainOrTradingChainOperationalRegion = data.ExcelTemplateForMTTP.ListRulesOfEligibleChainOrOperationalRegion;
            #endregion

            #region Customer MT-TP auto
            var listHeaderMTTPAuto = data.ExcelTemplateForMTTPAuto.ListHeaderTemplate;
            var listProgramCategoryMTTPAuto = data.ExcelTemplateForMTTPAuto.ListProgramCategoryTemplate;
            var listRulesOfProgramCategoryMTTPAuto = data.ExcelTemplateForMTTPAuto.ListRulesOfProgramCategory;
            var listRulesOfEligibilityOperationalRegionMTTPAuto = data.ExcelTemplateForMTTPAuto.ListRulesOfEligibilityOperationalRegion;
            var ListRulesOfEligibilityTradingChainMTTPAuto = data.ExcelTemplateForMTTPAuto.ListRulesOfEligibilityTradingChain;
            #endregion


            try
            {

                using (SpreadsheetDocument document = SpreadsheetDocument.Create(stream, SpreadsheetDocumentType.Workbook))
                {
                    WorkbookPart workbookPart = document.AddWorkbookPart();
                    workbookPart.Workbook = new Workbook();

                    WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                    worksheetPart.Worksheet = new Worksheet();

                    Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

                    Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Data" };

                    sheets.Append(sheet);

                    workbookPart.Workbook.Save();

                    SheetData sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());


                    if (!isMTTP)
                    {
                        if (trade == Trade.GT)
                        {
                            #region Customer GT
                            // Inserting each
                            foreach (var header in listHeader)
                            {
                                // Constructing header
                                Row rowHeader = new Row();

                                rowHeader.Append(
                                    ConstructCell("", CellValues.String),
                                    ConstructCell("Master Request Type", CellValues.String),
                                    ConstructCell("Description", CellValues.String),
                                    ConstructCell("Currency", CellValues.String),
                                    ConstructCell("Valid From", CellValues.String),
                                    ConstructCell("Valid To", CellValues.String),
                                    ConstructCell("Sales Organization", CellValues.String),
                                    ConstructCell("Distribution Channel", CellValues.String),
                                    ConstructCell("Division", CellValues.String),
                                    ConstructCell("Contract Valid From", CellValues.String),
                                    ConstructCell("Contract Valid To", CellValues.String),
                                    ConstructCell("Market Channel", CellValues.String),
                                    ConstructCell("Type(Contract/Promo)", CellValues.String),
                                    ConstructCell("External description", CellValues.String),
                                    ConstructCell("Spend Amount Currency", CellValues.String),
                                    ConstructCell("Payment terms", CellValues.String),
                                    ConstructCell("Payer", CellValues.String),
                                    ConstructCell("Contact Person", CellValues.String),
                                    ConstructCell("GM ID", CellValues.String)
                                );

                                // Insert the header row to the Sheet Data
                                sheetData.AppendChild(rowHeader);

                                var rowDetail = new Row();

                                rowDetail.Append(
                                    ConstructCell("$Hdr", CellValues.String),
                                    ConstructCell(header.MasterRequestType, CellValues.String),
                                    ConstructCell(header.Description, CellValues.String),
                                    ConstructCell(header.Currency, CellValues.String),
                                    ConstructCell(header.ValidFrom, CellValues.String),
                                    ConstructCell(header.ValidTo, CellValues.String),
                                    ConstructCell(header.SalesOrganization, CellValues.String),
                                    ConstructCell(header.DistributionChannel, CellValues.String),
                                    ConstructCell(header.Division, CellValues.String),
                                    ConstructCell(header.ContractValidFrom, CellValues.String),
                                    ConstructCell(header.ContractValidTo, CellValues.String),
                                    ConstructCell(header.MarketChannel, CellValues.String),
                                    ConstructCell(header.Type, CellValues.String),
                                    ConstructCell(header.ExternalDescription, CellValues.String),
                                    ConstructCell(header.SpendAmmountCurrency, CellValues.String),
                                    ConstructCell(header.PaymentTerms, CellValues.String),
                                    ConstructCell(header.Payer, CellValues.String),
                                    ConstructCell(header.ContactPerson, CellValues.String),
                                    ConstructCell(header.GMID, CellValues.String)
                                );

                                sheetData.AppendChild(rowDetail);

                                var listProgramCategoryFiltered = listProgramCategory.Where(x => x.RebateReceipnt.Equals(header.Payer)).ToList();

                                if (listProgramCategoryFiltered.Count > 0)
                                {
                                    Row rowHeaderProgramCategory = new Row();
                                    rowHeaderProgramCategory.Append(
                                        ConstructCell("", CellValues.String),
                                        ConstructCell("Promotion ID", CellValues.String),
                                        ConstructCell("Settlement Factor", CellValues.String),
                                        ConstructCell("Settlement Method", CellValues.String),
                                        ConstructCell("Spend Amount", CellValues.String),
                                        ConstructCell("Settlement Frequency", CellValues.String),
                                        ConstructCell("Payment Level", CellValues.String),
                                        ConstructCell("Rebate recipient", CellValues.String),
                                        ConstructCell("Valid From", CellValues.String),
                                        ConstructCell("Valid To", CellValues.String),
                                        ConstructCell("Program Category", CellValues.String),
                                        ConstructCell("Currency", CellValues.String)
                                    );
                                    sheetData.AppendChild(rowHeaderProgramCategory);
                                }

                                foreach (var programCategory in listProgramCategoryFiltered)
                                {
                                    // Constructing header
                                    Row rowDetailProgramCategory = new Row();
                                    rowDetailProgramCategory.Append(
                                        ConstructCell("$Pmp", CellValues.String),
                                        ConstructCell(programCategory.PromotionID, CellValues.String),
                                        ConstructCell(programCategory.SettlementFactor, CellValues.String),
                                        ConstructCell(programCategory.SettlementMethod, CellValues.String),
                                        ConstructCell(programCategory.SpendAmount, CellValues.String),
                                        ConstructCell(programCategory.SettlementFrequency, CellValues.String),
                                        ConstructCell(programCategory.PaymentLevel, CellValues.String),
                                        ConstructCell(programCategory.RebateReceipnt, CellValues.String),
                                        ConstructCell(programCategory.ValidFrom, CellValues.String),
                                        ConstructCell(programCategory.ValidTo, CellValues.String),
                                        ConstructCell(programCategory.ProgramCategory, CellValues.String),
                                        ConstructCell(programCategory.Currency, CellValues.String)
                                    );
                                    sheetData.AppendChild(rowDetailProgramCategory);
                                }


                                var listRulesOfProgramCategoryFiltered = listRulesOfProgramCategory.Where(x => x.OutletNumber.Equals(header.Payer));

                                if (listRulesOfProgramCategoryFiltered.Count() > 0)
                                {
                                    Row rowHeaderRulesOfProgramCategory = new Row();
                                    rowHeaderRulesOfProgramCategory.Append(
                                        ConstructCell("", CellValues.String),
                                        ConstructCell("Condition Type", CellValues.String),
                                        ConstructCell("Table", CellValues.String),
                                        ConstructCell("Trade Name", CellValues.String),
                                        ConstructCell("Operational Region", CellValues.String),
                                        ConstructCell("Sold-To Party", CellValues.String),
                                        ConstructCell("Material Group 1", CellValues.String),
                                        ConstructCell("Material Group 2", CellValues.String),
                                        ConstructCell("Material Group 3", CellValues.String),
                                        ConstructCell("Program ID", CellValues.String),
                                        ConstructCell("SKP Number", CellValues.String),
                                        ConstructCell("SKP Amount", CellValues.String),
                                        ConstructCell("Condition Currency", CellValues.String),
                                        ConstructCell("Rate", CellValues.String),
                                        ConstructCell("Per", CellValues.String),
                                        ConstructCell("Unit Of Measure", CellValues.String),
                                        ConstructCell("Valid From", CellValues.String),
                                        ConstructCell("Valid To", CellValues.String),
                                        ConstructCell("Accrue $", CellValues.String),
                                        ConstructCell("Acc $ UoM", CellValues.String),
                                        ConstructCell("Rebate Basis", CellValues.String),
                                        ConstructCell("Payout Basis", CellValues.String),
                                        ConstructCell("PY Sal Value", CellValues.String),
                                        ConstructCell("PY Agreement", CellValues.String),
                                        ConstructCell("Ref Currency", CellValues.String)
                                    );
                                    sheetData.AppendChild(rowHeaderRulesOfProgramCategory);
                                }

                                foreach (var rule in listRulesOfProgramCategoryFiltered)
                                {
                                    Row rowDetailRulesOfProgramCategory = new Row();
                                    rowDetailRulesOfProgramCategory.Append(
                                        ConstructCell("$Rule", CellValues.String),
                                        ConstructCell(rule.ConditionType, CellValues.String),
                                        ConstructCell(rule.Table, CellValues.String),
                                        ConstructCell(rule.TradeName, CellValues.String),
                                        ConstructCell(rule.OperationalRegion, CellValues.String),
                                        ConstructCell(rule.SoldToParty, CellValues.String),
                                        ConstructCell(rule.MaterialGroup1, CellValues.String),
                                        ConstructCell(rule.MaterialGroup2, CellValues.String),
                                        ConstructCell(rule.MaterialGroup3, CellValues.String),
                                        ConstructCell(rule.PromotionId, CellValues.String),
                                        ConstructCell(rule.SkpNumber, CellValues.String),
                                        ConstructCell(rule.SkpAmount, CellValues.String),
                                        ConstructCell(rule.ConditionCurrency, CellValues.String),
                                        ConstructCell(rule.Rate, CellValues.String),
                                        ConstructCell(rule.Per, CellValues.String),
                                        ConstructCell(rule.UnitOfMeasure, CellValues.String),
                                        ConstructCell(rule.ValidFrom, CellValues.String),
                                        ConstructCell(rule.ValidTo, CellValues.String),
                                        ConstructCell(rule.AccrueDollar, CellValues.String),
                                        ConstructCell(rule.AccDollarUOM, CellValues.String),
                                        ConstructCell(rule.RebateBasis, CellValues.String),
                                        ConstructCell(rule.PayoutBasis, CellValues.String),
                                        ConstructCell(rule.PYSalValue, CellValues.String),
                                        ConstructCell(rule.PYAgreement, CellValues.String),
                                        ConstructCell(rule.RefCurrency, CellValues.String)
                                    );
                                    sheetData.AppendChild(rowDetailRulesOfProgramCategory);

                                }


                                var listRulesOfEligibleSoldToFiltered = listRulesOfEligibleSoldTo.Where(x => x.OutletNumber.Equals(header.Payer));

                                if (listRulesOfEligibleSoldToFiltered.Count() > 0)
                                {
                                    Row rowHeaderRulesOfEligibleSoldTo = new Row();
                                    rowHeaderRulesOfEligibleSoldTo.Append(
                                        ConstructCell("", CellValues.String),
                                        ConstructCell("Condition Type", CellValues.String),
                                        ConstructCell("Table", CellValues.String),
                                        ConstructCell("Trade Name", CellValues.String),
                                        ConstructCell("Operational Region", CellValues.String),
                                        ConstructCell("Sold-To Party", CellValues.String),
                                        ConstructCell("Material Group 1", CellValues.String),
                                        ConstructCell("Material Group 2", CellValues.String),
                                        ConstructCell("Material Group 3", CellValues.String),
                                        ConstructCell("Program ID", CellValues.String),
                                        ConstructCell("SKP Number", CellValues.String),
                                        ConstructCell("SKP Amount", CellValues.String),
                                        ConstructCell("Condition Currency", CellValues.String),
                                        ConstructCell("Rate", CellValues.String),
                                        ConstructCell("Per", CellValues.String),
                                        ConstructCell("Unit Of Measure", CellValues.String),
                                        ConstructCell("Valid From", CellValues.String),
                                        ConstructCell("Valid To", CellValues.String),
                                        ConstructCell("Accrue $", CellValues.String),
                                        ConstructCell("Acc $ UoM", CellValues.String),
                                        ConstructCell("Rebate Basis", CellValues.String),
                                        ConstructCell("Payout Basis", CellValues.String),
                                        ConstructCell("PY Sal Value", CellValues.String),
                                        ConstructCell("PY Agreement", CellValues.String),
                                        ConstructCell("Ref Currency", CellValues.String)
                                    );
                                    sheetData.AppendChild(rowHeaderRulesOfEligibleSoldTo);
                                }
                                foreach (var rule in listRulesOfEligibleSoldToFiltered)
                                {
                                    Row rowDetailRulesOfEligibleSoldTo = new Row();
                                    rowDetailRulesOfEligibleSoldTo.Append(
                                        ConstructCell("$Rule", CellValues.String),
                                        ConstructCell(rule.ConditionType, CellValues.String),
                                        ConstructCell(rule.Table, CellValues.String),
                                        ConstructCell(rule.TradeName, CellValues.String),
                                        ConstructCell(rule.OperationalRegion, CellValues.String),
                                        ConstructCell(rule.SoldToParty, CellValues.String),
                                        ConstructCell(rule.MaterialGroup1, CellValues.String),
                                        ConstructCell(rule.MaterialGroup2, CellValues.String),
                                        ConstructCell(rule.MaterialGroup3, CellValues.String),
                                        ConstructCell(rule.PromotionId, CellValues.String),
                                        ConstructCell(rule.SkpNumber, CellValues.String),
                                        ConstructCell(rule.SkpAmount, CellValues.String),
                                        ConstructCell(rule.ConditionCurrency, CellValues.String),
                                        ConstructCell(rule.Rate, CellValues.String),
                                        ConstructCell(rule.Per, CellValues.String),
                                        ConstructCell(rule.UnitOfMeasure, CellValues.String),
                                        ConstructCell(rule.ValidFrom, CellValues.String),
                                        ConstructCell(rule.ValidTo, CellValues.String),
                                        ConstructCell(rule.AccrueDollar, CellValues.String),
                                        ConstructCell(rule.AccDollarUOM, CellValues.String),
                                        ConstructCell(rule.RebateBasis, CellValues.String),
                                        ConstructCell(rule.PayoutBasis, CellValues.String),
                                        ConstructCell(rule.PYSalValue, CellValues.String),
                                        ConstructCell(rule.PYAgreement, CellValues.String),
                                        ConstructCell(rule.RefCurrency, CellValues.String)
                                    );
                                    sheetData.AppendChild(rowDetailRulesOfEligibleSoldTo);
                                }



                                var listRulesOfSellOutFiltered = listRulesOfSellOut.Where(x => x.OutletNumber.Equals(header.Payer));

                                if (listRulesOfSellOutFiltered.Count() > 0)
                                {
                                    Row rowHeaderRulesOfSellOut = new Row();
                                    rowHeaderRulesOfSellOut.Append(
                                        ConstructCell("", CellValues.String),
                                        ConstructCell("Condition Type", CellValues.String),
                                        ConstructCell("Table", CellValues.String),
                                        ConstructCell("Trade Name", CellValues.String),
                                        ConstructCell("Operational Region", CellValues.String),
                                        ConstructCell("Sold-To Party", CellValues.String),
                                        ConstructCell("Material Group 1", CellValues.String),
                                        ConstructCell("Material Group 2", CellValues.String),
                                        ConstructCell("Material Group 3", CellValues.String),
                                        ConstructCell("Program ID", CellValues.String),
                                        ConstructCell("SKP Number", CellValues.String),
                                        ConstructCell("SKP Amount", CellValues.String),
                                        ConstructCell("Condition Currency", CellValues.String),
                                        ConstructCell("Rate", CellValues.String),
                                        ConstructCell("Per", CellValues.String),
                                        ConstructCell("Unit Of Measure", CellValues.String),
                                        ConstructCell("Valid From", CellValues.String),
                                        ConstructCell("Valid To", CellValues.String),
                                        ConstructCell("Accrue $", CellValues.String),
                                        ConstructCell("Acc $ UoM", CellValues.String),
                                        ConstructCell("Rebate Basis", CellValues.String),
                                        ConstructCell("Payout Basis", CellValues.String),
                                        ConstructCell("PY Sal Value", CellValues.String),
                                        ConstructCell("PY Agreement", CellValues.String),
                                        ConstructCell("Ref Currency", CellValues.String)
                                    );
                                    sheetData.AppendChild(rowHeaderRulesOfSellOut);

                                }

                                foreach (var rule in listRulesOfSellOutFiltered)
                                {
                                    Row rowDetailRulesOfSellOut = new Row();
                                    rowDetailRulesOfSellOut.Append(
                                        ConstructCell("$Rule", CellValues.String),
                                        ConstructCell(rule.ConditionType, CellValues.String),
                                        ConstructCell(rule.Table, CellValues.String),
                                        ConstructCell(rule.TradeName, CellValues.String),
                                        ConstructCell(rule.OperationalRegion, CellValues.String),
                                        ConstructCell(rule.SoldToParty, CellValues.String),
                                        ConstructCell(rule.MaterialGroup1, CellValues.String),
                                        ConstructCell(rule.MaterialGroup2, CellValues.String),
                                        ConstructCell(rule.MaterialGroup3, CellValues.String),
                                        ConstructCell(rule.PromotionId, CellValues.String),
                                        ConstructCell(rule.SkpNumber, CellValues.String),
                                        ConstructCell(rule.SkpAmount, CellValues.String),
                                        ConstructCell(rule.ConditionCurrency, CellValues.String),
                                        ConstructCell(rule.Rate, CellValues.String),
                                        ConstructCell(rule.Per, CellValues.String),
                                        ConstructCell(rule.UnitOfMeasure, CellValues.String),
                                        ConstructCell(rule.ValidFrom, CellValues.String),
                                        ConstructCell(rule.ValidTo, CellValues.String),
                                        ConstructCell(rule.AccrueDollar, CellValues.String),
                                        ConstructCell(rule.AccDollarUOM, CellValues.String),
                                        ConstructCell(rule.RebateBasis, CellValues.String),
                                        ConstructCell(rule.PayoutBasis, CellValues.String),
                                        ConstructCell(rule.PYSalValue, CellValues.String),
                                        ConstructCell(rule.PYAgreement, CellValues.String),
                                        ConstructCell(rule.RefCurrency, CellValues.String)
                                    );
                                    sheetData.AppendChild(rowDetailRulesOfSellOut);
                                }
                                Row rowEnd = new Row();
                                rowEnd.Append(ConstructCell("$END", CellValues.String));
                                sheetData.AppendChild(rowEnd);
                            }
                            #endregion
                        }
                        else
                        {
                            #region Customer MT-TT
                            // Inserting each
                            foreach (var header in listHeaderMTTT)
                            {
                                // Constructing header
                                Row rowHeader = new Row();

                                rowHeader.Append(
                                    ConstructCell("", CellValues.String),
                                    ConstructCell("Master Request Type", CellValues.String),
                                    ConstructCell("Description", CellValues.String),
                                    ConstructCell("Currency", CellValues.String),
                                    ConstructCell("Valid From", CellValues.String),
                                    ConstructCell("Valid To", CellValues.String),
                                    ConstructCell("Sales Organization", CellValues.String),
                                    ConstructCell("Distribution Channel", CellValues.String),
                                    ConstructCell("Division", CellValues.String),
                                    ConstructCell("Contract Valid From", CellValues.String),
                                    ConstructCell("Contract Valid To", CellValues.String),
                                    ConstructCell("Market Channel", CellValues.String),
                                    ConstructCell("Type(Contract/Promo)", CellValues.String),
                                    ConstructCell("External description", CellValues.String),
                                    ConstructCell("Spend Amount Currency", CellValues.String),
                                    ConstructCell("Payment terms", CellValues.String),
                                    ConstructCell("Payer", CellValues.String),
                                    ConstructCell("Contact Person", CellValues.String),
                                    ConstructCell("GM ID", CellValues.String)
                                );

                                // Insert the header row to the Sheet Data
                                sheetData.AppendChild(rowHeader);

                                var rowDetail = new Row();

                                rowDetail.Append(
                                    ConstructCell("$Hdr", CellValues.String),
                                    ConstructCell(header.MasterRequestType, CellValues.String),
                                    ConstructCell(header.Description, CellValues.String),
                                    ConstructCell(header.Currency, CellValues.String),
                                    ConstructCell(header.ValidFrom, CellValues.String),
                                    ConstructCell(header.ValidTo, CellValues.String),
                                    ConstructCell(header.SalesOrganization, CellValues.String),
                                    ConstructCell(header.DistributionChannel, CellValues.String),
                                    ConstructCell(header.Division, CellValues.String),
                                    ConstructCell(header.ContractValidFrom, CellValues.String),
                                    ConstructCell(header.ContractValidTo, CellValues.String),
                                    ConstructCell(header.MarketChannel, CellValues.String),
                                    ConstructCell(header.Type, CellValues.String),
                                    ConstructCell(header.ExternalDescription, CellValues.String),
                                    ConstructCell(header.SpendAmmountCurrency, CellValues.String),
                                    ConstructCell(header.PaymentTerms, CellValues.String),
                                    ConstructCell(header.Payer, CellValues.String),
                                    ConstructCell(header.ContactPerson, CellValues.String),
                                    ConstructCell(header.GMID, CellValues.String)
                                );

                                sheetData.AppendChild(rowDetail);

                                var listProgramCategoryFiltered = listProgramCategoryMTTT.Where(x => x.TradingChain.Equals(header.TradingChain) && x.RebateReceipnt.Equals(header.Payer)).ToList();

                                if (listProgramCategoryFiltered.Count > 0)
                                {
                                    Row rowHeaderProgramCategory = new Row();
                                    rowHeaderProgramCategory.Append(
                                        ConstructCell("", CellValues.String),
                                        ConstructCell("Promotion ID", CellValues.String),
                                        ConstructCell("Settlement Factor", CellValues.String),
                                        ConstructCell("Settlement Method", CellValues.String),
                                        ConstructCell("Spend Amount", CellValues.String),
                                        ConstructCell("Settlement Frequency", CellValues.String),
                                        ConstructCell("Payment Level", CellValues.String),
                                        ConstructCell("Rebate recipient", CellValues.String),
                                        ConstructCell("Valid From", CellValues.String),
                                        ConstructCell("Valid To", CellValues.String),
                                        ConstructCell("Program Category", CellValues.String),
                                        ConstructCell("Currency", CellValues.String)
                                    );
                                    sheetData.AppendChild(rowHeaderProgramCategory);
                                    foreach (var programCategory in listProgramCategoryFiltered)
                                    {
                                        // Constructing header
                                        Row rowDetailProgramCategory = new Row();
                                        rowDetailProgramCategory.Append(
                                            ConstructCell("$Pmp", CellValues.String),
                                            ConstructCell(programCategory.PromotionID, CellValues.String),
                                            ConstructCell(programCategory.SettlementFactor, CellValues.String),
                                            ConstructCell(programCategory.SettlementMethod, CellValues.String),
                                            ConstructCell(programCategory.SpendAmount, CellValues.String),
                                            ConstructCell(programCategory.SettlementFrequency, CellValues.String),
                                            ConstructCell(programCategory.PaymentLevel, CellValues.String),
                                            ConstructCell(programCategory.RebateReceipnt, CellValues.String),
                                            ConstructCell(programCategory.ValidFrom, CellValues.String),
                                            ConstructCell(programCategory.ValidTo, CellValues.String),
                                            ConstructCell(programCategory.ProgramCategory, CellValues.String),
                                            ConstructCell(programCategory.Currency, CellValues.String)
                                        );
                                        sheetData.AppendChild(rowDetailProgramCategory);
                                    }
                                }




                                var listRulesOfProgramCategoryFiltered = listRulesOfProgramCategoryMTTT.Where(x => x.TradingChain.Equals(header.TradingChain) && x.OutletNumber.Equals(header.Payer));

                                if (listRulesOfProgramCategoryFiltered.Count() > 0)
                                {
                                    Row rowHeaderRulesOfProgramCategory = new Row();
                                    rowHeaderRulesOfProgramCategory.Append(
                                        ConstructCell("", CellValues.String),
                                        ConstructCell("Condition Type", CellValues.String),
                                        ConstructCell("Table", CellValues.String),
                                        ConstructCell("Trade Name", CellValues.String),
                                        ConstructCell("Operational Region", CellValues.String),
                                        ConstructCell("Sold-To Party", CellValues.String),
                                        ConstructCell("Material Group 1", CellValues.String),
                                        ConstructCell("Material Group 2", CellValues.String),
                                        ConstructCell("Material Group 3", CellValues.String),
                                        ConstructCell("Program ID", CellValues.String),
                                        ConstructCell("SKP Number", CellValues.String),
                                        ConstructCell("SKP Amount", CellValues.String),
                                        ConstructCell("Condition Currency", CellValues.String),
                                        ConstructCell("Rate", CellValues.String),
                                        ConstructCell("Per", CellValues.String),
                                        ConstructCell("Unit Of Measure", CellValues.String),
                                        ConstructCell("Valid From", CellValues.String),
                                        ConstructCell("Valid To", CellValues.String),
                                        ConstructCell("Accrue $", CellValues.String),
                                        ConstructCell("Acc $ UoM", CellValues.String),
                                        ConstructCell("Rebate Basis", CellValues.String),
                                        ConstructCell("Payout Basis", CellValues.String),
                                        ConstructCell("PY Sal Value", CellValues.String),
                                        ConstructCell("PY Agreement", CellValues.String),
                                        ConstructCell("Ref Currency", CellValues.String)
                                    );
                                    sheetData.AppendChild(rowHeaderRulesOfProgramCategory);

                                    foreach (var rule in listRulesOfProgramCategoryFiltered)
                                    {
                                        Row rowDetailRulesOfProgramCategory = new Row();
                                        rowDetailRulesOfProgramCategory.Append(
                                            ConstructCell("$Rule", CellValues.String),
                                            ConstructCell(rule.ConditionType, CellValues.String),
                                            ConstructCell(rule.Table, CellValues.String),
                                            ConstructCell(rule.TradeName, CellValues.String),
                                            ConstructCell(rule.OperationalRegion, CellValues.String),
                                            ConstructCell(rule.SoldToParty, CellValues.String),
                                            ConstructCell(rule.MaterialGroup1, CellValues.String),
                                            ConstructCell(rule.MaterialGroup2, CellValues.String),
                                            ConstructCell(rule.MaterialGroup3, CellValues.String),
                                            ConstructCell(rule.PromotionId, CellValues.String),
                                            ConstructCell(rule.SkpNumber, CellValues.String),
                                            ConstructCell(rule.SkpAmount, CellValues.String),
                                            ConstructCell(rule.ConditionCurrency, CellValues.String),
                                            ConstructCell(rule.Rate, CellValues.String),
                                            ConstructCell(rule.Per, CellValues.String),
                                            ConstructCell(rule.UnitOfMeasure, CellValues.String),
                                            ConstructCell(rule.ValidFrom, CellValues.String),
                                            ConstructCell(rule.ValidTo, CellValues.String),
                                            ConstructCell(rule.AccrueDollar, CellValues.String),
                                            ConstructCell(rule.AccDollarUOM, CellValues.String),
                                            ConstructCell(rule.RebateBasis, CellValues.String),
                                            ConstructCell(rule.PayoutBasis, CellValues.String),
                                            ConstructCell(rule.PYSalValue, CellValues.String),
                                            ConstructCell(rule.PYAgreement, CellValues.String),
                                            ConstructCell(rule.RefCurrency, CellValues.String)
                                        );
                                        sheetData.AppendChild(rowDetailRulesOfProgramCategory);

                                    }
                                }

                                var listRulesOfEligibleTradingChainFiltered = listRulesOfEligibleTradingChain.Where(x => x.TradingChain.Equals(header.TradingChain) && x.OutletNumber.Equals(header.Payer));

                                if (listRulesOfEligibleTradingChainFiltered.Count() > 0)
                                {
                                    Row rowHeaderRulesOfEligibleTradingChain = new Row();
                                    rowHeaderRulesOfEligibleTradingChain.Append(
                                        ConstructCell("", CellValues.String),
                                        ConstructCell("Condition Type", CellValues.String),
                                        ConstructCell("Table", CellValues.String),
                                        ConstructCell("Trade Name", CellValues.String),
                                        ConstructCell("Operational Region", CellValues.String),
                                        ConstructCell("Sold-To Party", CellValues.String),
                                        ConstructCell("Material Group 1", CellValues.String),
                                        ConstructCell("Material Group 2", CellValues.String),
                                        ConstructCell("Material Group 3", CellValues.String),
                                        ConstructCell("Promotion ID", CellValues.String),
                                        ConstructCell("SKP Number", CellValues.String),
                                        ConstructCell("SKP Amount", CellValues.String),
                                        ConstructCell("Condition Currency", CellValues.String),
                                        ConstructCell("Rate", CellValues.String),
                                        ConstructCell("Per", CellValues.String),
                                        ConstructCell("Unit Of Measure", CellValues.String),
                                        ConstructCell("Valid From", CellValues.String),
                                        ConstructCell("Valid To", CellValues.String),
                                        ConstructCell("Accrue $", CellValues.String),
                                        ConstructCell("Dis$ UoM", CellValues.String),
                                        ConstructCell("Rebate Basis", CellValues.String),
                                        ConstructCell("Payout Basis", CellValues.String),
                                        ConstructCell("PY Sal Value", CellValues.String),
                                        ConstructCell("PY Agreement", CellValues.String),
                                        ConstructCell("Ref Currency", CellValues.String)
                                    );
                                    sheetData.AppendChild(rowHeaderRulesOfEligibleTradingChain);

                                    foreach (var rule in listRulesOfEligibleTradingChainFiltered)
                                    {
                                        Row rowDetailRulesOfEligibleTradingChain = new Row();
                                        rowDetailRulesOfEligibleTradingChain.Append(
                                            ConstructCell("$Rule", CellValues.String),
                                            ConstructCell(rule.ConditionType, CellValues.String),
                                            ConstructCell(rule.Table, CellValues.String),
                                            ConstructCell(rule.TradeName, CellValues.String),
                                            ConstructCell(rule.OperationalRegion, CellValues.String),
                                            ConstructCell(rule.SoldToParty, CellValues.String),
                                            ConstructCell(rule.MaterialGroup1, CellValues.String),
                                            ConstructCell(rule.MaterialGroup2, CellValues.String),
                                            ConstructCell(rule.MaterialGroup3, CellValues.String),
                                            ConstructCell(rule.PromotionId, CellValues.String),
                                            ConstructCell(rule.SkpNumber, CellValues.String),
                                            ConstructCell(rule.SkpAmount, CellValues.String),
                                            ConstructCell(rule.ConditionCurrency, CellValues.String),
                                            ConstructCell(rule.Rate, CellValues.String),
                                            ConstructCell(rule.Per, CellValues.String),
                                            ConstructCell(rule.UnitOfMeasure, CellValues.String),
                                            ConstructCell(rule.ValidFrom, CellValues.String),
                                            ConstructCell(rule.ValidTo, CellValues.String),
                                            ConstructCell(rule.AccrueDollar, CellValues.String),
                                            ConstructCell(rule.AccDollarUOM, CellValues.String),
                                            ConstructCell(rule.RebateBasis, CellValues.String),
                                            ConstructCell(rule.PayoutBasis, CellValues.String),
                                            ConstructCell(rule.PYSalValue, CellValues.String),
                                            ConstructCell(rule.PYAgreement, CellValues.String),
                                            ConstructCell(rule.RefCurrency, CellValues.String)
                                        );
                                        sheetData.AppendChild(rowDetailRulesOfEligibleTradingChain);
                                    }
                                }

                                var listRulesOfGrowthAllProductFiltered = listRulesOfGrowthAllProduct.Where(x => x.TradingChain.Equals(header.TradingChain) && x.OutletNumber.Equals(header.Payer));

                                if (listRulesOfGrowthAllProductFiltered.Count() > 0)
                                {
                                    Row rowHeaderRulesOfGrowthAllProduct = new Row();
                                    rowHeaderRulesOfGrowthAllProduct.Append(
                                        ConstructCell("", CellValues.String),
                                        ConstructCell("Condition Type", CellValues.String),
                                        ConstructCell("Table", CellValues.String),
                                        ConstructCell("Trade Name", CellValues.String),
                                        ConstructCell("Operational Region", CellValues.String),
                                        ConstructCell("Sold-To Party", CellValues.String),
                                        ConstructCell("Material Group 1", CellValues.String),
                                        ConstructCell("Material Group 2", CellValues.String),
                                        ConstructCell("Material Group 3", CellValues.String),
                                        ConstructCell("Program ID", CellValues.String),
                                        ConstructCell("SKP Number", CellValues.String),
                                        ConstructCell("SKP Amount", CellValues.String),
                                        ConstructCell("Condition Currency", CellValues.String),
                                        ConstructCell("Rate", CellValues.String),
                                        ConstructCell("Per", CellValues.String),
                                        ConstructCell("Unit Of Measure", CellValues.String),
                                        ConstructCell("Valid From", CellValues.String),
                                        ConstructCell("Valid To", CellValues.String),
                                        ConstructCell("Accrue $", CellValues.String),
                                        ConstructCell("Dis$ UoM", CellValues.String),
                                        ConstructCell("Rebate Basis", CellValues.String),
                                        ConstructCell("Payout Basis", CellValues.String),
                                        ConstructCell("PY Sal Value", CellValues.String),
                                        ConstructCell("PY Agreement", CellValues.String),
                                        ConstructCell("Ref Currency", CellValues.String)
                                    );
                                    sheetData.AppendChild(rowHeaderRulesOfGrowthAllProduct);

                                    foreach (var rule in listRulesOfGrowthAllProductFiltered)
                                    {
                                        Row rowDetailRulesOfGrowthAllProduct = new Row();
                                        rowDetailRulesOfGrowthAllProduct.Append(
                                            ConstructCell("$Rule", CellValues.String),
                                            ConstructCell(rule.ConditionType, CellValues.String),
                                            ConstructCell(rule.Table, CellValues.String),
                                            ConstructCell(rule.TradeName, CellValues.String),
                                            ConstructCell(rule.OperationalRegion, CellValues.String),
                                            ConstructCell(rule.SoldToParty, CellValues.String),
                                            ConstructCell(rule.MaterialGroup1, CellValues.String),
                                            ConstructCell(rule.MaterialGroup2, CellValues.String),
                                            ConstructCell(rule.MaterialGroup3, CellValues.String),
                                            ConstructCell(rule.PromotionId, CellValues.String),
                                            ConstructCell(rule.SkpNumber, CellValues.String),
                                            ConstructCell(rule.SkpAmount, CellValues.String),
                                            ConstructCell(rule.ConditionCurrency, CellValues.String),
                                            ConstructCell(rule.Rate, CellValues.String),
                                            ConstructCell(rule.Per, CellValues.String),
                                            ConstructCell(rule.UnitOfMeasure, CellValues.String),
                                            ConstructCell(rule.ValidFrom, CellValues.String),
                                            ConstructCell(rule.ValidTo, CellValues.String),
                                            ConstructCell(rule.AccrueDollar, CellValues.String),
                                            ConstructCell(rule.AccDollarUOM, CellValues.String),
                                            ConstructCell(rule.RebateBasis, CellValues.String),
                                            ConstructCell(rule.PayoutBasis, CellValues.String),
                                            ConstructCell(rule.PYSalValue, CellValues.String),
                                            ConstructCell(rule.PYAgreement, CellValues.String),
                                            ConstructCell(rule.RefCurrency, CellValues.String)
                                        );
                                        sheetData.AppendChild(rowDetailRulesOfGrowthAllProduct);
                                    }
                                }

                                var listRulesOfGrowthEstmAccrualFiltered = listRulesOfGrowthEstmAccrual.Where(x => x.TradingChain.Equals(header.TradingChain) && x.OutletNumber.Equals(header.Payer));

                                if (listRulesOfGrowthEstmAccrualFiltered.Count() > 0)
                                {
                                    Row rowHeaderRulesOfGrowthEstmAccrual = new Row();
                                    rowHeaderRulesOfGrowthEstmAccrual.Append(
                                        ConstructCell("", CellValues.String),
                                        ConstructCell("Condition Type", CellValues.String),
                                        ConstructCell("Table", CellValues.String),
                                        ConstructCell("Trade Name", CellValues.String),
                                        ConstructCell("Operational Region", CellValues.String),
                                        ConstructCell("Sold-To Party", CellValues.String),
                                        ConstructCell("Material Group 1", CellValues.String),
                                        ConstructCell("Material Group 2", CellValues.String),
                                        ConstructCell("Material Group 3", CellValues.String),
                                        ConstructCell("Program ID", CellValues.String),
                                        ConstructCell("SKP Number", CellValues.String),
                                        ConstructCell("SKP Amount", CellValues.String),
                                        ConstructCell("Condition Currency", CellValues.String),
                                        ConstructCell("Rate", CellValues.String),
                                        ConstructCell("Per", CellValues.String),
                                        ConstructCell("Unit Of Measure", CellValues.String),
                                        ConstructCell("Valid From", CellValues.String),
                                        ConstructCell("Valid To", CellValues.String),
                                        ConstructCell("Accrue $", CellValues.String),
                                        ConstructCell("Dis$ UoM", CellValues.String),
                                        ConstructCell("Rebate Basis", CellValues.String),
                                        ConstructCell("Payout Basis", CellValues.String),
                                        ConstructCell("PY Sal Value", CellValues.String),
                                        ConstructCell("PY Agreement", CellValues.String),
                                        ConstructCell("Ref Currency", CellValues.String)
                                    );
                                    sheetData.AppendChild(rowHeaderRulesOfGrowthEstmAccrual);

                                    foreach (var rule in listRulesOfGrowthEstmAccrualFiltered)
                                    {
                                        Row rowDetailRulesOfGrowthEstmAccrual = new Row();
                                        rowDetailRulesOfGrowthEstmAccrual.Append(
                                            ConstructCell("$Rule", CellValues.String),
                                            ConstructCell(rule.ConditionType, CellValues.String),
                                            ConstructCell(rule.Table, CellValues.String),
                                            ConstructCell(rule.TradeName, CellValues.String),
                                            ConstructCell(rule.OperationalRegion, CellValues.String),
                                            ConstructCell(rule.SoldToParty, CellValues.String),
                                            ConstructCell(rule.MaterialGroup1, CellValues.String),
                                            ConstructCell(rule.MaterialGroup2, CellValues.String),
                                            ConstructCell(rule.MaterialGroup3, CellValues.String),
                                            ConstructCell(rule.PromotionId, CellValues.String),
                                            ConstructCell(rule.SkpNumber, CellValues.String),
                                            ConstructCell(rule.SkpAmount, CellValues.String),
                                            ConstructCell(rule.ConditionCurrency, CellValues.String),
                                            ConstructCell(rule.Rate, CellValues.String),
                                            ConstructCell(rule.Per, CellValues.String),
                                            ConstructCell(rule.UnitOfMeasure, CellValues.String),
                                            ConstructCell(rule.ValidFrom, CellValues.String),
                                            ConstructCell(rule.ValidTo, CellValues.String),
                                            ConstructCell(rule.AccrueDollar, CellValues.String),
                                            ConstructCell(rule.AccDollarUOM, CellValues.String),
                                            ConstructCell(rule.RebateBasis, CellValues.String),
                                            ConstructCell(rule.PayoutBasis, CellValues.String),
                                            ConstructCell(rule.PYSalValue, CellValues.String),
                                            ConstructCell(rule.PYAgreement, CellValues.String),
                                            ConstructCell(rule.RefCurrency, CellValues.String)
                                        );
                                        sheetData.AppendChild(rowDetailRulesOfGrowthEstmAccrual);
                                    }
                                }

                                var listRulesOfGrowthCalculationFiltered = listRulesOfGrowthCalculation.Where(x => x.TradingChain.Equals(header.TradingChain) && x.OutletNumber.Equals(header.Payer));

                                if (listRulesOfGrowthCalculationFiltered.Count() > 0)
                                {
                                    Row rowHeaderRulesOfGrowthCalculation = new Row();
                                    rowHeaderRulesOfGrowthCalculation.Append(
                                        ConstructCell("", CellValues.String),
                                        ConstructCell("Condition Type", CellValues.String),
                                        ConstructCell("Table", CellValues.String),
                                        ConstructCell("Trade Name", CellValues.String),
                                        ConstructCell("Operational Region", CellValues.String),
                                        ConstructCell("Sold-To Party", CellValues.String),
                                        ConstructCell("Material Group 1", CellValues.String),
                                        ConstructCell("Material Group 2", CellValues.String),
                                        ConstructCell("Material Group 3", CellValues.String),
                                        ConstructCell("Program ID", CellValues.String),
                                        ConstructCell("SKP Number", CellValues.String),
                                        ConstructCell("SKP Amount", CellValues.String),
                                        ConstructCell("Condition Currency", CellValues.String),
                                        ConstructCell("Rate", CellValues.String),
                                        ConstructCell("Per", CellValues.String),
                                        ConstructCell("Unit Of Measure", CellValues.String),
                                        ConstructCell("Valid From", CellValues.String),
                                        ConstructCell("Valid To", CellValues.String),
                                        ConstructCell("Accrue $", CellValues.String),
                                        ConstructCell("Dis$ UoM", CellValues.String),
                                        ConstructCell("Rebate Basis", CellValues.String),
                                        ConstructCell("Payout Basis", CellValues.String),
                                        ConstructCell("PY Sal Value", CellValues.String),
                                        ConstructCell("PY Agreement", CellValues.String),
                                        ConstructCell("Ref Currency", CellValues.String)
                                    );
                                    sheetData.AppendChild(rowHeaderRulesOfGrowthCalculation);

                                    foreach (var rule in listRulesOfGrowthCalculationFiltered)
                                    {
                                        Row rowDetailRulesOfGrowthCalculation = new Row();
                                        rowDetailRulesOfGrowthCalculation.Append(
                                            ConstructCell("$Rule", CellValues.String),
                                            ConstructCell(rule.ConditionType, CellValues.String),
                                            ConstructCell(rule.Table, CellValues.String),
                                            ConstructCell(rule.TradeName, CellValues.String),
                                            ConstructCell(rule.OperationalRegion, CellValues.String),
                                            ConstructCell(rule.SoldToParty, CellValues.String),
                                            ConstructCell(rule.MaterialGroup1, CellValues.String),
                                            ConstructCell(rule.MaterialGroup2, CellValues.String),
                                            ConstructCell(rule.MaterialGroup3, CellValues.String),
                                            ConstructCell(rule.PromotionId, CellValues.String),
                                            ConstructCell(rule.SkpNumber, CellValues.String),
                                            ConstructCell(rule.SkpAmount, CellValues.String),
                                            ConstructCell(rule.ConditionCurrency, CellValues.String),
                                            ConstructCell(rule.Rate, CellValues.String),
                                            ConstructCell(rule.Per, CellValues.String),
                                            ConstructCell(rule.UnitOfMeasure, CellValues.String),
                                            ConstructCell(rule.ValidFrom, CellValues.String),
                                            ConstructCell(rule.ValidTo, CellValues.String),
                                            ConstructCell(rule.AccrueDollar, CellValues.String),
                                            ConstructCell(rule.AccDollarUOM, CellValues.String),
                                            ConstructCell(rule.RebateBasis, CellValues.String),
                                            ConstructCell(rule.PayoutBasis, CellValues.String),
                                            ConstructCell(rule.PYSalValue, CellValues.String),
                                            ConstructCell(rule.PYAgreement, CellValues.String),
                                            ConstructCell(rule.RefCurrency, CellValues.String)
                                        );
                                        sheetData.AppendChild(rowDetailRulesOfGrowthCalculation);
                                    }
                                }

                                var listRulesOfGrowthTierFiltered = listRulesOfGrowthTier.Where(x => x.TradingChain.Equals(header.TradingChain) && x.OutletNumber.Equals(header.Payer));

                                if (listRulesOfGrowthTierFiltered.Count() > 0)
                                {
                                    Row rowHeaderRulesOfGrowthTier = new Row();
                                    rowHeaderRulesOfGrowthTier.Append(
                                        ConstructCell("", CellValues.String),
                                        ConstructCell("Condition Type", CellValues.String),
                                        ConstructCell("Table", CellValues.String),
                                        ConstructCell("Trade Name", CellValues.String),
                                        ConstructCell("Operational Region", CellValues.String),
                                        ConstructCell("Sold-To Party", CellValues.String),
                                        ConstructCell("Material Group 1", CellValues.String),
                                        ConstructCell("Material Group 2", CellValues.String),
                                        ConstructCell("Material Group 3", CellValues.String),
                                        ConstructCell("Program ID", CellValues.String),
                                        ConstructCell("SKP Number", CellValues.String),
                                        ConstructCell("SKP Amount", CellValues.String),
                                        ConstructCell("Condition Currency", CellValues.String),
                                        ConstructCell("Rate", CellValues.String),
                                        ConstructCell("Per", CellValues.String),
                                        ConstructCell("Unit Of Measure", CellValues.String),
                                        ConstructCell("Valid From", CellValues.String),
                                        ConstructCell("Valid To", CellValues.String),
                                        ConstructCell("Accrue $", CellValues.String),
                                        ConstructCell("Dis$ UoM", CellValues.String),
                                        ConstructCell("Rebate Basis", CellValues.String),
                                        ConstructCell("Payout Basis", CellValues.String),
                                        ConstructCell("PY Sal Value", CellValues.String),
                                        ConstructCell("PY Agreement", CellValues.String),
                                        ConstructCell("Ref Currency", CellValues.String)
                                    );
                                    sheetData.AppendChild(rowHeaderRulesOfGrowthTier);

                                    foreach (var rule in listRulesOfGrowthTierFiltered)
                                    {
                                        Row rowDetailRulesOfGrowthTier = new Row();
                                        rowDetailRulesOfGrowthTier.Append(
                                            ConstructCell("$Rule", CellValues.String),
                                            ConstructCell(rule.ConditionType, CellValues.String),
                                            ConstructCell(rule.Table, CellValues.String),
                                            ConstructCell(rule.TradeName, CellValues.String),
                                            ConstructCell(rule.OperationalRegion, CellValues.String),
                                            ConstructCell(rule.SoldToParty, CellValues.String),
                                            ConstructCell(rule.MaterialGroup1, CellValues.String),
                                            ConstructCell(rule.MaterialGroup2, CellValues.String),
                                            ConstructCell(rule.MaterialGroup3, CellValues.String),
                                            ConstructCell(rule.PromotionId, CellValues.String),
                                            ConstructCell(rule.SkpNumber, CellValues.String),
                                            ConstructCell(rule.SkpAmount, CellValues.String),
                                            ConstructCell(rule.ConditionCurrency, CellValues.String),
                                            ConstructCell(rule.Rate, CellValues.String),
                                            ConstructCell(rule.Per, CellValues.String),
                                            ConstructCell(rule.UnitOfMeasure, CellValues.String),
                                            ConstructCell(rule.ValidFrom, CellValues.String),
                                            ConstructCell(rule.ValidTo, CellValues.String),
                                            ConstructCell(rule.AccrueDollar, CellValues.String),
                                            ConstructCell(rule.AccDollarUOM, CellValues.String),
                                            ConstructCell(rule.RebateBasis, CellValues.String),
                                            ConstructCell(rule.PayoutBasis, CellValues.String),
                                            ConstructCell(rule.PYSalValue, CellValues.String),
                                            ConstructCell(rule.PYAgreement, CellValues.String),
                                            ConstructCell(rule.RefCurrency, CellValues.String)
                                        );
                                        sheetData.AppendChild(rowDetailRulesOfGrowthTier);
                                    }
                                }

                                var listRulesOfGrowthScaleFiltered = listRulesOfGrowthScale.Where(x => x.TradingChain.Equals(header.TradingChain) && x.OutletNumber.Equals(header.Payer));

                                if (listRulesOfGrowthScaleFiltered.Count() > 0)
                                {
                                    Row rowHeaderRulesOfGrowthScale = new Row();
                                    rowHeaderRulesOfGrowthScale.Append(
                                        ConstructCell("", CellValues.String),
                                        ConstructCell("Growth Percentage", CellValues.String),
                                        ConstructCell("Scale currency", CellValues.String),
                                        ConstructCell("Scale quantity", CellValues.String),
                                        ConstructCell("Scale unit of meas.", CellValues.String),
                                        ConstructCell("Condition currency", CellValues.String),
                                        ConstructCell("Condition rate", CellValues.String)
                                    );
                                    sheetData.AppendChild(rowHeaderRulesOfGrowthScale);

                                    foreach (var rule in listRulesOfGrowthScaleFiltered)
                                    {
                                        Row rowDetailRulesOfGrowthScale = new Row();
                                        rowDetailRulesOfGrowthScale.Append(
                                            ConstructCell("$Scales", CellValues.String),
                                            ConstructCell(rule.GrowthPercentage, CellValues.String),
                                            ConstructCell(rule.ScaleCurrency, CellValues.String),
                                            ConstructCell(rule.ScaleQuantity, CellValues.String),
                                            ConstructCell(rule.ScaleUnitOfMeasure, CellValues.String),
                                            ConstructCell(rule.ConditionCurrency, CellValues.String),
                                            ConstructCell(rule.ConditionRate, CellValues.String)
                                        );
                                        sheetData.AppendChild(rowDetailRulesOfGrowthScale);
                                    }
                                }

                                Row rowEnd = new Row();
                                rowEnd.Append(ConstructCell("$END", CellValues.String));
                                sheetData.AppendChild(rowEnd);
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        #region Customer MT-TP

                        foreach (var header in listHeaderMTTP)
                        {
                            // Constructing header
                            Row rowHeader = new Row();

                            rowHeader.Append(
                                ConstructCell("", CellValues.String),
                                ConstructCell("Master Request Type", CellValues.String),
                                ConstructCell("Description", CellValues.String),
                                ConstructCell("Currency", CellValues.String),
                                ConstructCell("Valid From", CellValues.String),
                                ConstructCell("Valid To", CellValues.String),
                                ConstructCell("Sales Organization", CellValues.String),
                                ConstructCell("Distribution Channel", CellValues.String),
                                ConstructCell("Division", CellValues.String),
                                ConstructCell("Contract Valid From", CellValues.String),
                                ConstructCell("Contract Valid To", CellValues.String),
                                ConstructCell("Market Channel", CellValues.String),
                                ConstructCell("Type(Contract/Promo)", CellValues.String),
                                ConstructCell("External description", CellValues.String),
                                ConstructCell("Spend Amount Currency", CellValues.String),
                                ConstructCell("Payment terms", CellValues.String),
                                ConstructCell("Payer", CellValues.String),
                                ConstructCell("Contact Person", CellValues.String),
                                ConstructCell("GM ID", CellValues.String)
                            );

                            // Insert the header row to the Sheet Data
                            sheetData.AppendChild(rowHeader);


                            var rowDetail = new Row();

                            rowDetail.Append(
                                ConstructCell("$Hdr", CellValues.String),
                                ConstructCell(header.MasterRequestType, CellValues.String),
                                ConstructCell(header.Description, CellValues.String),
                                ConstructCell(header.Currency, CellValues.String),
                                ConstructCell(header.ValidFrom, CellValues.String),
                                ConstructCell(header.ValidTo, CellValues.String),
                                ConstructCell(header.SalesOrganization, CellValues.String),
                                ConstructCell(header.DistributionChannel, CellValues.String),
                                ConstructCell(header.Division, CellValues.String),
                                ConstructCell(header.ContractValidFrom, CellValues.String),
                                ConstructCell(header.ContractValidTo, CellValues.String),
                                ConstructCell(header.MarketChannel, CellValues.String),
                                ConstructCell(header.Type, CellValues.String),
                                ConstructCell(header.ExternalDescription, CellValues.String),
                                ConstructCell(header.SpendAmmountCurrency, CellValues.String),
                                ConstructCell(header.PaymentTerms, CellValues.String),
                                ConstructCell(header.Payer, CellValues.String),
                                ConstructCell(header.ContactPerson, CellValues.String),
                                ConstructCell(header.GMID, CellValues.String)
                            );

                            sheetData.AppendChild(rowDetail);

                            var listProgramCategoryFiltered = listProgramCategoryMTTP.Where(x => x.Index.Equals(header.Index)).ToList();

                            if (listProgramCategoryFiltered.Count > 0)
                            {
                                Row rowHeaderProgramCategory = new Row();
                                rowHeaderProgramCategory.Append(
                                    ConstructCell("", CellValues.String),
                                    ConstructCell("Promotion ID", CellValues.String),
                                    ConstructCell("Settlement Factor", CellValues.String),
                                    ConstructCell("Settlement Method", CellValues.String),
                                    ConstructCell("Spend Amount", CellValues.String),
                                    ConstructCell("Settlement Frequency", CellValues.String),
                                    ConstructCell("Payment Level", CellValues.String),
                                    ConstructCell("Rebate recipient", CellValues.String),
                                    ConstructCell("Valid From", CellValues.String),
                                    ConstructCell("Valid To", CellValues.String),
                                    ConstructCell("Program Category", CellValues.String),
                                    ConstructCell("Currency", CellValues.String)
                                );
                                sheetData.AppendChild(rowHeaderProgramCategory);

                                foreach (var programCategory in listProgramCategoryFiltered)
                                {
                                    // Constructing header
                                    Row rowDetailProgramCategory = new Row();
                                    rowDetailProgramCategory.Append(
                                        ConstructCell("$Pmp", CellValues.String),
                                        ConstructCell(programCategory.PromotionID, CellValues.String),
                                        ConstructCell(programCategory.SettlementFactor, CellValues.String),
                                        ConstructCell(programCategory.SettlementMethod, CellValues.String),
                                        ConstructCell(programCategory.SpendAmount, CellValues.String),
                                        ConstructCell(programCategory.SettlementFrequency, CellValues.String),
                                        ConstructCell(programCategory.PaymentLevel, CellValues.String),
                                        ConstructCell(programCategory.RebateReceipnt, CellValues.String),
                                        ConstructCell(programCategory.ValidFrom, CellValues.String),
                                        ConstructCell(programCategory.ValidTo, CellValues.String),
                                        ConstructCell(programCategory.ProgramCategory, CellValues.String),
                                        ConstructCell(programCategory.Currency, CellValues.String)
                                    );
                                    sheetData.AppendChild(rowDetailProgramCategory);
                                }
                            }

                            var listRulesOfProgramCategoryFiltered = listRulesOfProgramCategoryMTTP.Where(x => x.Index.Equals(header.Index)).ToList();

                            if (listRulesOfProgramCategoryFiltered.Count > 0)
                            {
                                Row rowHeaderRulesOfProgramCategory = new Row();
                                rowHeaderRulesOfProgramCategory.Append(
                                    ConstructCell("", CellValues.String),
                                    ConstructCell("Condition Type", CellValues.String),
                                    ConstructCell("Table", CellValues.String),
                                    ConstructCell("Trade Name", CellValues.String),
                                    ConstructCell("Operational Region", CellValues.String),
                                    ConstructCell("Sold-To Party", CellValues.String),
                                    ConstructCell("Material Group 1", CellValues.String),
                                    ConstructCell("Material Group 2", CellValues.String),
                                    ConstructCell("Material Group 3", CellValues.String),
                                    ConstructCell("Program ID", CellValues.String),
                                    ConstructCell("SKP Number", CellValues.String),
                                    ConstructCell("SKP Amount", CellValues.String),
                                    ConstructCell("Condition Currency", CellValues.String),
                                    ConstructCell("Rate", CellValues.String),
                                    ConstructCell("Per", CellValues.String),
                                    ConstructCell("Unit Of Measure", CellValues.String),
                                    ConstructCell("Valid From", CellValues.String),
                                    ConstructCell("Valid To", CellValues.String),
                                    ConstructCell("Accrue $", CellValues.String),
                                    ConstructCell("Acc $ UoM", CellValues.String),
                                    ConstructCell("Rebate Basis", CellValues.String),
                                    ConstructCell("Payout Basis", CellValues.String),
                                    ConstructCell("PY Sal Value", CellValues.String),
                                    ConstructCell("PY Agreement", CellValues.String),
                                    ConstructCell("Ref Currency", CellValues.String)
                                );
                                sheetData.AppendChild(rowHeaderRulesOfProgramCategory);

                                foreach (var rule in listRulesOfProgramCategoryFiltered)
                                {
                                    Row rowDetailRulesOfSellOut = new Row();
                                    rowDetailRulesOfSellOut.Append(
                                        ConstructCell("$Rule", CellValues.String),
                                        ConstructCell(rule.ConditionType, CellValues.String),
                                        ConstructCell(rule.Table, CellValues.String),
                                        ConstructCell(rule.TradeName, CellValues.String),
                                        ConstructCell(rule.OperationalRegion, CellValues.String),
                                        ConstructCell(rule.SoldToParty, CellValues.String),
                                        ConstructCell(rule.MaterialGroup1, CellValues.String),
                                        ConstructCell(rule.MaterialGroup2, CellValues.String),
                                        ConstructCell(rule.MaterialGroup3, CellValues.String),
                                        ConstructCell(rule.PromotionId, CellValues.String),
                                        ConstructCell(rule.SkpNumber, CellValues.String),
                                        ConstructCell(rule.SkpAmount, CellValues.String),
                                        ConstructCell(rule.ConditionCurrency, CellValues.String),
                                        ConstructCell(rule.Rate, CellValues.String),
                                        ConstructCell(rule.Per, CellValues.String),
                                        ConstructCell(rule.UnitOfMeasure, CellValues.String),
                                        ConstructCell(rule.ValidFrom, CellValues.String),
                                        ConstructCell(rule.ValidTo, CellValues.String),
                                        ConstructCell(rule.AccrueDollar, CellValues.String),
                                        ConstructCell(rule.AccDollarUOM, CellValues.String),
                                        ConstructCell(rule.RebateBasis, CellValues.String),
                                        ConstructCell(rule.PayoutBasis, CellValues.String),
                                        ConstructCell(rule.PYSalValue, CellValues.String),
                                        ConstructCell(rule.PYAgreement, CellValues.String),
                                        ConstructCell(rule.RefCurrency, CellValues.String)
                                    );
                                    sheetData.AppendChild(rowDetailRulesOfSellOut);
                                }


                            }
                            var listRulesOfEligibleChainOrTradingChainOperationalRegionFiltered = listRulesOfEligibleChainOrTradingChainOperationalRegion.Where(x => x.Index.Equals(header.Index)).ToList();

                            if (listRulesOfEligibleChainOrTradingChainOperationalRegionFiltered.Count > 0)
                            {
                                Row rowHeaderRulesOfEligibleChainOrTradingChain = new Row();
                                rowHeaderRulesOfEligibleChainOrTradingChain.Append(
                                    ConstructCell("", CellValues.String),
                                    ConstructCell("Condition Type", CellValues.String),
                                    ConstructCell("Table", CellValues.String),
                                    ConstructCell("Trade Name", CellValues.String),
                                    ConstructCell("Operational Region", CellValues.String),
                                    ConstructCell("Sold-To Party", CellValues.String),
                                    ConstructCell("Material Group 1", CellValues.String),
                                    ConstructCell("Material Group 2", CellValues.String),
                                    ConstructCell("Material Group 3", CellValues.String),
                                    ConstructCell("Promotion ID", CellValues.String),
                                    ConstructCell("SKP Number", CellValues.String),
                                    ConstructCell("SKP Amount", CellValues.String),
                                    ConstructCell("Condition Currency", CellValues.String),
                                    ConstructCell("Rate", CellValues.String),
                                    ConstructCell("Per", CellValues.String),
                                    ConstructCell("Unit Of Measure", CellValues.String),
                                    ConstructCell("Valid From", CellValues.String),
                                    ConstructCell("Valid To", CellValues.String),
                                    ConstructCell("Accrue $", CellValues.String),
                                    ConstructCell("Dis$ UoM", CellValues.String),
                                    ConstructCell("Rebate Basis", CellValues.String),
                                    ConstructCell("Payout Basis", CellValues.String),
                                    ConstructCell("PY Sal Value", CellValues.String),
                                    ConstructCell("PY Agreement", CellValues.String),
                                    ConstructCell("Ref Currency", CellValues.String)
                                );
                                sheetData.AppendChild(rowHeaderRulesOfEligibleChainOrTradingChain);

                                foreach (var rule in listRulesOfEligibleChainOrTradingChainOperationalRegionFiltered)
                                {
                                    Row rowDetailRulesOfEligibleChainOrTradingChain = new Row();
                                    rowDetailRulesOfEligibleChainOrTradingChain.Append(
                                        ConstructCell("$Rule", CellValues.String),
                                        ConstructCell(rule.ConditionType, CellValues.String),
                                        ConstructCell(rule.Table, CellValues.String),
                                        ConstructCell(rule.TradeName, CellValues.String),
                                        ConstructCell(rule.OperationalRegion, CellValues.String),
                                        ConstructCell(rule.SoldToParty, CellValues.String),
                                        ConstructCell(rule.MaterialGroup1, CellValues.String),
                                        ConstructCell(rule.MaterialGroup2, CellValues.String),
                                        ConstructCell(rule.MaterialGroup3, CellValues.String),
                                        ConstructCell(rule.PromotionId, CellValues.String),
                                        ConstructCell(rule.SkpNumber, CellValues.String),
                                        ConstructCell(rule.SkpAmount, CellValues.String),
                                        ConstructCell(rule.ConditionCurrency, CellValues.String),
                                        ConstructCell(rule.Rate, CellValues.String),
                                        ConstructCell(rule.Per, CellValues.String),
                                        ConstructCell(rule.UnitOfMeasure, CellValues.String),
                                        ConstructCell(rule.ValidFrom, CellValues.String),
                                        ConstructCell(rule.ValidTo, CellValues.String),
                                        ConstructCell(rule.AccrueDollar, CellValues.String),
                                        ConstructCell(rule.DisDollarUOM, CellValues.String),
                                        ConstructCell(rule.RebateBasis, CellValues.String),
                                        ConstructCell(rule.PayoutBasis, CellValues.String),
                                        ConstructCell(rule.PYSalValue, CellValues.String),
                                        ConstructCell(rule.PYAgreement, CellValues.String),
                                        ConstructCell(rule.RefCurrency, CellValues.String)
                                    );
                                    sheetData.AppendChild(rowDetailRulesOfEligibleChainOrTradingChain);
                                }
                            }

                            Row rowEnd = new Row();
                            rowEnd.Append(ConstructCell("$END", CellValues.String));
                            sheetData.AppendChild(rowEnd);
                        }
                        #endregion

                        #region Customer MT-TP Auto

                        foreach (var header in listHeaderMTTPAuto)
                        {
                            // Constructing header
                            Row rowHeader = new Row();

                            rowHeader.Append(
                                ConstructCell("", CellValues.String),
                                ConstructCell("Master Request Type", CellValues.String),
                                ConstructCell("Description", CellValues.String),
                                ConstructCell("External description", CellValues.String),
                                ConstructCell("Valid From", CellValues.String),
                                ConstructCell("Valid To", CellValues.String),
                                ConstructCell("Contract Valid From", CellValues.String),
                                ConstructCell("Contract Valid To", CellValues.String),
                                ConstructCell("Market Channel", CellValues.String),
                                ConstructCell("Type(Contract/Promo)", CellValues.String),
                                ConstructCell("Payment terms", CellValues.String),
                                ConstructCell("Payer", CellValues.String),
                                ConstructCell("Contact Person", CellValues.String),
                                ConstructCell("Status Flow Approver", CellValues.String)

                            );

                            // Insert the header row to the Sheet Data
                            sheetData.AppendChild(rowHeader);


                            var rowDetail = new Row();

                            rowDetail.Append(
                                ConstructCell("$Hdr", CellValues.String),
                                ConstructCell(header.MasterRequestType, CellValues.String),
                                ConstructCell(header.Description, CellValues.String),
                                ConstructCell(header.ExternalDescription, CellValues.String),
                                ConstructCell(header.ValidFrom, CellValues.String),
                                ConstructCell(header.ValidTo, CellValues.String),
                                ConstructCell(header.ContractValidFrom, CellValues.String),
                                ConstructCell(header.ContractValidTo, CellValues.String),
                                ConstructCell(header.MarketChannel, CellValues.String),
                                ConstructCell(header.Type, CellValues.String),
                                ConstructCell(header.PaymentTerms, CellValues.String),
                                ConstructCell(header.Payer, CellValues.String),
                                ConstructCell(header.ContactPerson, CellValues.String),
                                ConstructCell(header.StatusFlowApprover, CellValues.String)
                            );

                            sheetData.AppendChild(rowDetail);

                            var listProgramCategoryFiltered = listProgramCategoryMTTPAuto.Where(x => x.Index.Equals(header.Index)).ToList();

                            if (listProgramCategoryFiltered.Count > 0)
                            {
                                Row rowHeaderProgramCategory = new Row();
                                rowHeaderProgramCategory.Append(
                                    ConstructCell("", CellValues.String),
                                    ConstructCell("Program Ctegory", CellValues.String),
                                    ConstructCell("Settlement Factor", CellValues.String),
                                    ConstructCell("Settlement Method", CellValues.String),
                                    ConstructCell("Spend Amount", CellValues.String),
                                    ConstructCell("Rebate recipient", CellValues.String),
                                    ConstructCell("Valid From", CellValues.String),
                                    ConstructCell("Valid To", CellValues.String)

                                );
                                sheetData.AppendChild(rowHeaderProgramCategory);

                                foreach (var programCategory in listProgramCategoryFiltered)
                                {
                                    // Constructing header
                                    Row rowDetailProgramCategory = new Row();
                                    rowDetailProgramCategory.Append(
                                        ConstructCell("$Pmp", CellValues.String),
                                        ConstructCell(programCategory.ProgramCategory, CellValues.String),
                                        ConstructCell(programCategory.SettlementFactor, CellValues.String),
                                        ConstructCell(programCategory.SettlementMethod, CellValues.String),
                                        ConstructCell(programCategory.SpendAmount, CellValues.String),
                                        ConstructCell(programCategory.RebateReceipnt, CellValues.String),
                                        ConstructCell(programCategory.ValidFrom, CellValues.String),
                                        ConstructCell(programCategory.ValidTo, CellValues.String)

                                    );
                                    sheetData.AppendChild(rowDetailProgramCategory);
                                }
                            }

                            var listRulesOfProgramCategoryFiltered = listRulesOfProgramCategoryMTTPAuto.Where(x => x.Index.Equals(header.Index)).ToList();

                            if (listRulesOfProgramCategoryFiltered.Count > 0)
                            {
                                Row rowHeaderRulesOfProgramCategory = new Row();
                                rowHeaderRulesOfProgramCategory.Append(
                                    ConstructCell("", CellValues.String),
                                    ConstructCell("Price Sheet Description", CellValues.String),
                                    ConstructCell("Table", CellValues.String),
                                    ConstructCell("Trade Name", CellValues.String),
                                    ConstructCell("Operational Region", CellValues.String),
                                    ConstructCell("Promotion ID", CellValues.String),
                                    ConstructCell("Material Group 1", CellValues.String),
                                    ConstructCell("Accrue $", CellValues.String),
                                    ConstructCell("Dis$ UOM", CellValues.String),
                                    ConstructCell("Valid From", CellValues.String),
                                    ConstructCell("Valid To", CellValues.String)
                                );
                                sheetData.AppendChild(rowHeaderRulesOfProgramCategory);

                                foreach (var rule in listRulesOfProgramCategoryFiltered)
                                {
                                    Row rowDetailRulesOfSellOut = new Row();
                                    rowDetailRulesOfSellOut.Append(
                                        ConstructCell("$Rule", CellValues.String),
                                        ConstructCell(rule.PriceSheetDescription, CellValues.String),
                                        ConstructCell(rule.Table, CellValues.String),
                                        ConstructCell(rule.TradeName, CellValues.String),
                                        ConstructCell(rule.OperationalRegion, CellValues.String),
                                        ConstructCell(rule.PromotionId, CellValues.String),
                                        ConstructCell(rule.MaterialGroup1, CellValues.String),
                                        ConstructCell(rule.AccrueDollar, CellValues.String),
                                        ConstructCell(rule.DisDollarUOM, CellValues.String),
                                        ConstructCell(rule.ValidFrom, CellValues.String),
                                        ConstructCell(rule.ValidTo, CellValues.String)
                                    );
                                    sheetData.AppendChild(rowDetailRulesOfSellOut);
                                }


                            }
                            var listRulesOfEligibilityOperationalRegionMTTPAutoFiltered = listRulesOfEligibilityOperationalRegionMTTPAuto.Where(x => x.Index.Equals(header.Index)).ToList();

                            if (listRulesOfEligibilityOperationalRegionMTTPAutoFiltered.Count > 0)
                            {
                                Row rowHeaderRulesOfEligibilityOperationalRegionMTTPAuto = new Row();
                                rowHeaderRulesOfEligibilityOperationalRegionMTTPAuto.Append(
                                    ConstructCell("", CellValues.String),
                                    ConstructCell("Price Sheet Description", CellValues.String),
                                    ConstructCell("Table", CellValues.String),
                                    ConstructCell("Trade Name", CellValues.String),
                                    ConstructCell("Operational Region", CellValues.String),
                                    ConstructCell("Promotion ID", CellValues.String),
                                    ConstructCell("Material Group 1", CellValues.String),
                                    ConstructCell("Accrue $", CellValues.String),
                                    ConstructCell("Dis$ UoM", CellValues.String),
                                    ConstructCell("Valid From", CellValues.String),
                                    ConstructCell("Valid To", CellValues.String)
                                );
                                sheetData.AppendChild(rowHeaderRulesOfEligibilityOperationalRegionMTTPAuto);

                                foreach (var rule in listRulesOfEligibilityOperationalRegionMTTPAutoFiltered)
                                {
                                    Row rowDetailRulesOfEligibilityOperationalRegionMTTPAuto = new Row();
                                    rowDetailRulesOfEligibilityOperationalRegionMTTPAuto.Append(
                                        ConstructCell("$Rule", CellValues.String),
                                        ConstructCell(rule.PriceSheetDescription, CellValues.String),
                                        ConstructCell(rule.Table, CellValues.String),
                                        ConstructCell(rule.TradeName, CellValues.String),
                                        ConstructCell(rule.OperationalRegion, CellValues.String),
                                        ConstructCell(rule.PromotionID, CellValues.String),
                                        ConstructCell(rule.MaterialGroup1, CellValues.String),
                                        ConstructCell(rule.AccrueDollar, CellValues.String),
                                        ConstructCell(rule.DisDollarUOM, CellValues.String),
                                        ConstructCell(rule.ValidFrom, CellValues.String),
                                        ConstructCell(rule.ValidTo, CellValues.String)
                                    );
                                    sheetData.AppendChild(rowDetailRulesOfEligibilityOperationalRegionMTTPAuto);
                                }
                            }


                            var listRulesOfEligibilityTradingChainTTPAutoFiltered = ListRulesOfEligibilityTradingChainMTTPAuto.Where(x => x.Index.Equals(header.Index)).ToList();

                            if (listRulesOfEligibilityTradingChainTTPAutoFiltered.Count > 0)
                            {
                                Row rowHeaderRulesOfEligibilityTradingChainTTPAuto = new Row();
                                rowHeaderRulesOfEligibilityTradingChainTTPAuto.Append(
                                    ConstructCell("", CellValues.String),
                                    ConstructCell("Price Sheet Description", CellValues.String),
                                    ConstructCell("Table", CellValues.String),
                                    ConstructCell("Trade Name", CellValues.String),
                                    ConstructCell("Operational Region", CellValues.String),
                                    ConstructCell("Promotion ID", CellValues.String),
                                    ConstructCell("Material Group 1", CellValues.String),
                                    ConstructCell("Accrue $", CellValues.String),
                                    ConstructCell("Dis$ UoM", CellValues.String),
                                    ConstructCell("Valid From", CellValues.String),
                                    ConstructCell("Valid To", CellValues.String)
                                );
                                sheetData.AppendChild(rowHeaderRulesOfEligibilityTradingChainTTPAuto);

                                foreach (var rule in listRulesOfEligibilityTradingChainTTPAutoFiltered)
                                {
                                    Row rowDetailRulesOfEligibilityTradingChainTTPAuto = new Row();
                                    rowDetailRulesOfEligibilityTradingChainTTPAuto.Append(
                                        ConstructCell("$Rule", CellValues.String),
                                        ConstructCell(rule.PriceSheetDescription, CellValues.String),
                                        ConstructCell(rule.Table, CellValues.String),
                                        ConstructCell(rule.TradeName, CellValues.String),
                                        ConstructCell(rule.OperationalRegion, CellValues.String),
                                        ConstructCell(rule.PromotionID, CellValues.String),
                                        ConstructCell(rule.MaterialGroup1, CellValues.String),
                                        ConstructCell(rule.AccrueDollar, CellValues.String),
                                        ConstructCell(rule.DisDollarUOM, CellValues.String),
                                        ConstructCell(rule.ValidFrom, CellValues.String),
                                        ConstructCell(rule.ValidTo, CellValues.String)
                                    );
                                    sheetData.AppendChild(rowDetailRulesOfEligibilityTradingChainTTPAuto);
                                }
                            }
                            Row rowEnd = new Row();
                            rowEnd.Append(ConstructCell("$END", CellValues.String));
                            sheetData.AppendChild(rowEnd);
                        }
                        #endregion
                    }

                    worksheetPart.Worksheet.Save();
                    stream.Seek(0, SeekOrigin.Begin);
                    return stream;
                }

            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public ExportExcelWrapper ReadExcel(HttpPostedFileBase hpf, string agreementType)
        {
            var listErrorLog = new List<string>();
            var DEFAULT_VALUE = new DefaultValue();
            var listUploadTemplate = new List<UploadTemplate>();
            StringBuilder listError = new StringBuilder();
            bool isDataValid = true;
            Trade trade;
            TradeType tradeType;
            int resultCount = 0;

            try
            {
                //System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
                //if (hfc.Count == 0)
                //{
                //    var message = "File has not been uploaded";
                //    return;
                //}
                //HttpPostedFile hpf = hfc[0];

                string fileExtension = System.IO.Path.GetExtension(hpf.FileName);
                if (fileExtension.ToLower() != ".xlsx")
                {
                    listErrorLog.Add("File is not excel");
                    return new ExportExcelWrapper
                    {
                        IsSucced = false,
                        ErrorLog = listErrorLog

                    };
                }

                if (hpf.ContentLength > 0)
                {

                    using (SpreadsheetDocument doc = SpreadsheetDocument.Open(hpf.InputStream, false)) //add reference WindowsBase
                    {
                        WorkbookPart workBookPart = doc.WorkbookPart;
                        IEnumerable<Sheet> sheets = workBookPart.Workbook.Descendants<Sheet>();
                        Sheet sheet = sheets.First();
                        WorksheetPart workSheet = ((WorksheetPart)workBookPart.GetPartById(sheet.Id));
                        string rowNum;

                        using (OpenXmlReader reader = OpenXmlReader.Create(workSheet))
                        {
                            while (reader.Read())
                            {
                                if (reader.ElementType == typeof(Row))
                                {
                                    do
                                    {
                                        if (reader.HasAttributes)
                                        {
                                            rowNum = reader.Attributes.First(a => a.LocalName == "r").Value;
                                            StringBuilder currentListError = new StringBuilder();


                                            //row ketiga di ignore karena di isi data header dan row ke 5 karena judul kolom
                                            if (Convert.ToInt32(rowNum) > 2)
                                            {
                                                Row r = (Row)reader.LoadCurrentElement();
                                                List<string> DataRows = new List<string>();
                                                var cellEnumerator = ReadExcelHelper.GetExcelCellEnumerator(r);

                                                while (cellEnumerator.MoveNext())
                                                {
                                                    var cell = cellEnumerator.Current;

                                                    var value = ReadExcelHelper.ReadExcelCell(cell, workBookPart).Trim();
                                                    DataRows.Add(value);
                                                }
                                                try
                                                {
                                                    #region Validasi
                                                    //validasi awal
                                                    //validasi mandatoryfield
                                                    string outletNumber = DataRows[2];
                                                    string mtGt = DataRows[5];
                                                    string type = DataRows[6];
                                                    string tradingChain = DataRows[7];
                                                    string programCategory = DataRows[10];
                                                    string programCat = DataRows[11];
                                                    string budgetPerProgramCat = DataRows[12];
                                                    string periodStart = DataRows[13];
                                                    string periodEnd = DataRows[14];
                                                    string paymentTerms = DataRows[26];
                                                    string settlement = DataRows[27];


                                                    if (string.IsNullOrEmpty(outletNumber) || string.IsNullOrWhiteSpace(outletNumber))
                                                    {
                                                        string message = "Outlet Number Kosong";
                                                        listError.Append("\nERROR ON CELL " + rowNum + ": " + message + " ");
                                                        listErrorLog.Add("ERROR ON CELL " + rowNum + ": " + message);
                                                        isDataValid = false;
                                                    }
                                                    if (string.IsNullOrEmpty(mtGt) || string.IsNullOrWhiteSpace(mtGt))
                                                    {
                                                        string message = "MT/GT Kosong";
                                                        listError.Append("\nERROR ON CELL " + rowNum + ": " + message + " ");
                                                        listErrorLog.Add("ERROR ON CELL " + rowNum + ": " + message);
                                                        isDataValid = false;
                                                    }
                                                    if (string.IsNullOrEmpty(type) || string.IsNullOrWhiteSpace(type))
                                                    {
                                                        string message = "Type Kosong";
                                                        listError.Append("\nERROR ON CELL " + rowNum + ": " + message + " ");
                                                        listErrorLog.Add("ERROR ON CELL " + rowNum + ": " + message);
                                                        isDataValid = false;
                                                    }
                                                    if (string.IsNullOrEmpty(tradingChain) || string.IsNullOrWhiteSpace(tradingChain))
                                                    {
                                                        string message = "Trading Chain Kosong";
                                                        listError.Append("\nERROR ON CELL " + rowNum + ": " + message + " ");
                                                        listErrorLog.Add("ERROR ON CELL " + rowNum + ": " + message);
                                                        isDataValid = false;
                                                    }

                                                    if (string.IsNullOrEmpty(programCategory) || string.IsNullOrWhiteSpace(programCategory))
                                                    {
                                                        string message = "Program Category Kosong";
                                                        listError.Append("\nERROR ON CELL " + rowNum + ": " + message + " ");
                                                        listErrorLog.Add("ERROR ON CELL " + rowNum + ": " + message);
                                                        isDataValid = false;
                                                    }
                                                    if (string.IsNullOrEmpty(programCat) || string.IsNullOrWhiteSpace(programCat))
                                                    {
                                                        string message = "Program Cat Kosong";
                                                        listError.Append("\nERROR ON CELL " + rowNum + ": " + message + " ");
                                                        listErrorLog.Add("ERROR ON CELL " + rowNum + ": " + message);
                                                        isDataValid = false;
                                                    }
                                                    if (string.IsNullOrEmpty(budgetPerProgramCat) || string.IsNullOrWhiteSpace(budgetPerProgramCat))
                                                    {
                                                        string message = "Budget Per Program Cat Kosong";
                                                        listError.Append("\nERROR ON CELL " + rowNum + ": " + message + " ");
                                                        listErrorLog.Add("ERROR ON CELL " + rowNum + ": " + message);

                                                        isDataValid = false;
                                                    }
                                                    if (string.IsNullOrEmpty(periodStart) || string.IsNullOrWhiteSpace(periodStart))
                                                    {
                                                        string message = "Period Start Kosong";
                                                        listError.Append("\nERROR ON CELL " + rowNum + ": " + message + " ");
                                                        listErrorLog.Add("ERROR ON CELL " + rowNum + ": " + message);
                                                        isDataValid = false;
                                                    }
                                                    if (string.IsNullOrEmpty(periodEnd) || string.IsNullOrWhiteSpace(periodEnd))
                                                    {
                                                        string message = "Period End Kosong";
                                                        listError.Append("\nERROR ON CELL " + rowNum + ": " + message + " ");
                                                        listErrorLog.Add("ERROR ON CELL " + rowNum + ": " + message);
                                                        isDataValid = false;
                                                    }
                                                    if (string.IsNullOrEmpty(paymentTerms) || string.IsNullOrWhiteSpace(paymentTerms))
                                                    {
                                                        string message = "Payment Term Kosong";
                                                        listError.Append("\nERROR ON CELL " + rowNum + ": " + message + " ");
                                                        listErrorLog.Add("ERROR ON CELL " + rowNum + ": " + message);
                                                        isDataValid = false;
                                                    }
                                                    if (string.IsNullOrEmpty(settlement) || string.IsNullOrWhiteSpace(settlement))
                                                    {
                                                        string message = "Settlement Kosong";
                                                        listError.Append("\nERROR ON CELL " + rowNum + ": " + message + " ");
                                                        listErrorLog.Add("ERROR ON CELL " + rowNum + ": " + message);
                                                        isDataValid = false;
                                                    }

                                                    #endregion

                                                    //dicomment dijadiin helper

                                                    //var decimalBudgetPerProgramCat = Math.Round(decimal.Parse(DataRows[12]));
                                                    ////string strBudgetPerProgramCat = string.Format("{0:#,0}", decimalBudgetPerProgramCat);
                                                    //string strBudgetPerProgramCat = decimalBudgetPerProgramCat.ToString().Replace(",", "");

                                                    //var decimalPreviousYearSalesValue = Math.Round(decimal.Parse(DataRows[19]));
                                                    //string strPreviousYearSalesValue = decimalPreviousYearSalesValue.ToString().Replace(",", "");

                                                    listUploadTemplate.Add(
                                                        new UploadTemplate
                                                        {
                                                            No = Convert.ToInt32(DataRows[0]),
                                                            OutletNumber = DataRows[1],
                                                            CustGroupName = DataRows[2],
                                                            LegalContractYes = DataRows[3],
                                                            LegalContractNo = DataRows[4],
                                                            MTGT = DataRows[5],
                                                            Type = DataRows[6],
                                                            TradingChain = DataRows[7],
                                                            EligibleOperationalRegion = DataRows[8],
                                                            PPA = DataRows[9],
                                                            ProgramCategory = DataRows[10],
                                                            ProgramCat = DataRows[11],
                                                            BudgetPerProgramCat = ConvertHelper.GetRoundDecimal(DataRows[12]),//helper baru untuk round decimal
                                                            PeriodStart = DataRows[13].Length == 5 ? ConvertHelper.GetCustomDate(DataRows[13]) : DataRows[13],//kadang ada date yang ke return angka panjangnya 5, jadi di format dulu
                                                            PeriodEnd = DataRows[14].Length == 5 ? ConvertHelper.GetCustomDate(DataRows[14]) : DataRows[14],
                                                            RatePercentage = DataRows[15] == "" ? "" : ConvertHelper.GetCustomDecimal(DataRows[15],3),//helper baru untuk ambil 3 angka di belakang koma
                                                            RateIDR = DataRows[16] ,
                                                            MaterialGroup1Desc = DataRows[17],
                                                            MaterialGroup1 = DataRows[18],
                                                            PreviousYearSalesValue = DataRows[19] == "" ? "" : ConvertHelper.GetRoundDecimal(DataRows[19]),
                                                            GrowthScal1GrowthPercentage = DataRows[20],
                                                            GrowthScal1RatePercentage = DataRows[21] == "" ? "": ConvertHelper.GetCustomDecimal(DataRows[21], 2),
                                                            GrowthScal2GrowthPercentage = DataRows[22],
                                                            GrowthScal2RatePercentage = DataRows[23] == "" ? "" : ConvertHelper.GetCustomDecimal(DataRows[23], 2),
                                                            GrowthScal3GrowthPercentage = DataRows[24],
                                                            GrowthScal3RatePercentage = DataRows[25] == "" ? "" : ConvertHelper.GetCustomDecimal(DataRows[25], 2),
                                                            PaymentTerms = DataRows[26],
                                                            Settlement = DataRows[27],
                                                            GMID = DataRows[28],
                                                            SellOutRatePercentage = DataRows[29],
                                                            SellOutRateIDR = DataRows[30],
                                                        }
                                                    );
                                                }

                                                catch (Exception ex)
                                                {
                                                    isDataValid = false;
                                                    string message = ex.Message;
                                                    string undefinedColumn = "Index was out of range";
                                                    var result = message.Contains(undefinedColumn);
                                                    if (result)
                                                    {
                                                        listError.Append("\nThere's column that different from Excel Template");
                                                        listErrorLog.Add("There's column that different from Excel Template");
                                                    }
                                                    else
                                                    {
                                                        listError.Append(ex.Message);
                                                        listErrorLog.Add(ex.ToString());
                                                    }
                                                }
                                            }

                                        }
                                    } while (reader.ReadNextSibling());
                                    break;
                                }
                            }
                        }
                    }


                    if (isDataValid)
                    {



                        #region Customer GT
                        var listHeader = new List<HeaderTemplate>();
                        var listProgramCategory = new List<ProgramCategoryTemplate>();
                        var listRulesOfProgramCategory = new List<RulesTemplate>();
                        var listRulesOfEligibleSoldTo = new List<RulesTemplate>();
                        var listRulesOfSellOut = new List<RulesTemplate>();
                        #endregion

                        #region Customer MT-TT
                        var listHeaderMTTT = new List<HeaderTemplate>();
                        var listProgramCategoryMTTT = new List<ProgramCategoryTemplate>();
                        var listRulesOfProgramCategoryMTTT = new List<RulesMTTemplate>();
                        var listRulesOfEligibleTradingChainMTTT = new List<RulesMTTemplate>();
                        var listRulesOfGrowthAllProductMTTT = new List<RulesMTTemplate>();
                        var listRulesOfGrowthEstmAccrualMTTT = new List<RulesMTTemplate>();
                        var listRulesOfGrowthCalculationMTTT = new List<RulesMTTemplate>();
                        var listRulesOfGrowthTierMTTT = new List<RulesMTTemplate>();
                        var listRulesOfGrowthScaleMTTT = new List<GrowthScale>();
                        #endregion

                        #region Customer MT-TP
                        var listHeaderMTTP = new List<HeaderTemplate>();
                        var listProgramCategoryMTTP = new List<ProgramCategoryTemplate>();
                        var listRulesOfProgramCategoryMTTP = new List<RulesMTTemplate>();
                        var listRulesOfEligibleChainOrTradingChainOperationalRegion = new List<RulesMTTemplate>();
                        #endregion

                        #region Customer MT-TP Auto
                        var listHeaderMTTPAuto = new List<HeaderTemplate>();
                        var listProgramCategoryMTTPAuto = new List<ProgramCategoryTemplate>();
                        var listRulesOfProgramCategoryMTTPAuto = new List<RulesMTTemplate>();
                        var listRulesOfEligiblityOperationalRegionMTTPAuto = new List<RulesMTTemplate>();
                        var listRulesOfEligiblityTradingChainMTTPAuto = new List<RulesMTTemplate>();
                        #endregion

                        bool isMTTP;

                        #region validasi radio button

                        if (listUploadTemplate[0].MTGT == Trade.GT.ToString())
                        {
                            trade = Trade.GT;
                            isMTTP = false;
                            tradeType = TradeType.GT;

                            if (agreementType == "overAndAbove")
                            {
                                var listErrorLogBusinessType = new List<string>() {
                                    "Type Agreement is not valid"
                                };
                                return new ExportExcelWrapper
                                {
                                    IsSucced = false,
                                    ErrorLog = listErrorLogBusinessType,
                                };
                            }

                        }
                        else
                        {
                            trade = Trade.MT;
                            if (listUploadTemplate[0].Type.Substring(0, 2) == TradeType.TT.ToString())
                            {

                                if (agreementType == "overAndAbove")
                                {
                                    var listErrorLogBusinessType = new List<string>() {
                                        "Business Type is not valid"
                                    };
                                    return new ExportExcelWrapper
                                    {
                                        IsSucced = false,
                                        ErrorLog = listErrorLogBusinessType,
                                    };
                                }

                                tradeType = TradeType.TT;
                                isMTTP = false;


                            }
                            else
                            {
                                tradeType = TradeType.TP;
                                isMTTP = true;
                            }
                        }
                        #endregion

                        if (!isMTTP)
                        {
                            #region distinct by customer
                            var distinctListCustomer = listUploadTemplate.Distinct(new CustomerComparer()).ToList();
                            int countCustomer = distinctListCustomer.Count();
                            var listCustomer = new List<string>();
                            for (int i = 0; i < countCustomer; i++)
                            {
                                listCustomer.Add(distinctListCustomer[i].OutletNumber);
                            }
                            #endregion

                            #region distinct by trading chain and outlet number hanya untuk MT-TT
                            var distinctListHaderMTTT = listUploadTemplate.Select(r => new { r.OutletNumber, r.TradingChain }).Distinct().ToList();
                            var listFilterHeaderMTTT = new List<GroupByMTTT>();
                            foreach (var header in distinctListHaderMTTT)
                            {
                                listFilterHeaderMTTT.Add(new GroupByMTTT { OutletNumber = header.OutletNumber, TradingChain = header.TradingChain });
                            }

                            #endregion


                            //grouped by customer 
                            //this will handle for Customer GT and Customer MT-TT
                            foreach (var customer in listCustomer)
                            {

                                var listDataByCustomer = listUploadTemplate.Where(x => x.OutletNumber.Equals(customer)).ToList();

                                if (listDataByCustomer.Count >= 1)
                                {
                                    //Customer GT
                                    if (listDataByCustomer[0].MTGT.ToLower() == Trade.GT.ToString().ToLower())
                                    {

                                        #region Customer GT
                                        //insert data ke variabel grouped by customer
                                        string masterRequestType = DEFAULT_VALUE.MasterRequestType;
                                        string tempDescription = listDataByCustomer[0].CustGroupName;
                                        int lengthTempDescription = tempDescription.Length > DEFAULT_VALUE.MaxLengthDescription ? DEFAULT_VALUE.MaxLengthDescription : tempDescription.Length;
                                        string description = tempDescription.Substring(0, lengthTempDescription);
                                        string currency = DEFAULT_VALUE.CurrencyH;
                                        string validFrom = listDataByCustomer[0].PeriodStart;
                                        string validTo = listDataByCustomer[0].PeriodEnd;
                                        string salesOrganization = DEFAULT_VALUE.SalesOrganization;
                                        string distributionChannel = DEFAULT_VALUE.DistributionChannel;
                                        string division = DEFAULT_VALUE.Division;
                                        string contractValidFrom = listDataByCustomer[0].PeriodStart;
                                        string contractValidTo = listDataByCustomer[0].PeriodEnd;
                                        string marketChannel = listDataByCustomer[0].MTGT;
                                        string tempType2 = listDataByCustomer[0].Type;
                                        int lengthTempTempType = tempType2.Length > DEFAULT_VALUE.MaxLengthType ? DEFAULT_VALUE.MaxLengthType : tempType2.Length;
                                        string type2 = listDataByCustomer[0].Type.Substring(0, lengthTempTempType);
                                        string externalDescription = DEFAULT_VALUE.ExternalDescription;
                                        string spendAmmountCurrency = DEFAULT_VALUE.SpendAmountCurrencyMTTP;
                                        string tempPaymentTerms = listDataByCustomer[0].PaymentTerms;
                                        int lengthTempPaymentTerms = tempType2.Length > DEFAULT_VALUE.MaxLengthPaymentTerms ? DEFAULT_VALUE.MaxLengthPaymentTerms : tempType2.Length;
                                        string paymentTerms2 = listDataByCustomer[0].PaymentTerms.Substring(0, lengthTempPaymentTerms);
                                        string payer = listDataByCustomer[0].OutletNumber;
                                        string contactPerson = DEFAULT_VALUE.ContactPerson;
                                        string gmId = listDataByCustomer[0].GMID;

                                        var headerTemplate = new HeaderTemplate
                                        {
                                            MasterRequestType = masterRequestType,
                                            Description = description,
                                            Currency = currency,
                                            ValidFrom = validFrom,
                                            ValidTo = validTo,
                                            SalesOrganization = salesOrganization,
                                            DistributionChannel = distributionChannel,
                                            Division = division,
                                            ContractValidFrom = contractValidFrom,
                                            ContractValidTo = contractValidTo,
                                            MarketChannel = marketChannel,
                                            Type = type2,
                                            ExternalDescription = externalDescription,
                                            SpendAmmountCurrency = spendAmmountCurrency,
                                            PaymentTerms = paymentTerms2,
                                            Payer = payer,
                                            ContactPerson = contactPerson,
                                            GMID = gmId
                                        };
                                        listHeader.Add(headerTemplate);
                                        //count customer gt
                                        resultCount++;

                                        var listProgramCategoryByCustomer = listUploadTemplate.Where(x => x.OutletNumber.Equals(customer)).Distinct(new ProgramCatComparer());
                                        foreach (var programCategory in listProgramCategoryByCustomer)
                                        {
                                            listProgramCategory.Add(
                                                new ProgramCategoryTemplate
                                                {
                                                    PromotionID = programCategory.ProgramCat,
                                                    SettlementFactor = DEFAULT_VALUE.SettlementFactor,
                                                    SettlementMethod = programCategory.Settlement.Substring(0, 2),
                                                    SpendAmount = programCategory.BudgetPerProgramCat,
                                                    SettlementFrequency = DEFAULT_VALUE.SettlementFrequency,
                                                    PaymentLevel = DEFAULT_VALUE.PaymentLevel,
                                                    RebateReceipnt = programCategory.OutletNumber,
                                                    ValidFrom = programCategory.PeriodStart,
                                                    ValidTo = programCategory.PeriodEnd,
                                                    ProgramCategory = programCategory.ProgramCat,
                                                    Currency = DEFAULT_VALUE.CurrencyPC
                                                }
                                            );
                                        }

                                        var listRulesByCustomer = listUploadTemplate.Where(x => x.OutletNumber.Equals(customer));
                                        foreach (var rule in listRulesByCustomer)
                                        {
                                            listRulesOfProgramCategory.Add(
                                                new RulesTemplate
                                                {
                                                    ConditionType = rule.MaterialGroup1 == "" ? DEFAULT_VALUE.ConditionTypeEmpty : DEFAULT_VALUE.ConditionTypeNotEmpty,
                                                    Table = DEFAULT_VALUE.Table,
                                                    TradeName = "",
                                                    OperationalRegion = "",
                                                    SoldToParty = "",
                                                    MaterialGroup1 = rule.MaterialGroup1,
                                                    MaterialGroup2 = "",
                                                    MaterialGroup3 = "",
                                                    PromotionId = rule.ProgramCat,
                                                    SkpNumber = "",
                                                    SkpAmount = "",
                                                    ConditionCurrency = rule.RatePercentage != "" ? DEFAULT_VALUE.ConditionCurrencyPC : "",
                                                    Rate = rule.RatePercentage,
                                                    Per = "",
                                                    UnitOfMeasure = "",
                                                    ValidFrom = rule.PeriodStart,
                                                    ValidTo = rule.PeriodEnd,
                                                    AccrueDollar = rule.RateIDR,
                                                    RebateBasis = "",
                                                    PayoutBasis = "",
                                                    PYSalValue = "",
                                                    PYAgreement = "",
                                                    RefCurrency = rule.MaterialGroup1 ==  "" ? "" : "IDR",
                                                    OutletNumber = customer
                                                }
                                            );
                                        }

                                        var top1ListRulesByCustomer = listRulesByCustomer.FirstOrDefault();
                                        if (top1ListRulesByCustomer != null)
                                        {

                                            listRulesOfEligibleSoldTo.Add(
                                                new RulesTemplate
                                                {
                                                    ConditionType = DEFAULT_VALUE.ConditionTypeEST,
                                                    Table = DEFAULT_VALUE.TableEST,
                                                    TradeName = "",
                                                    OperationalRegion = "",
                                                    SoldToParty = top1ListRulesByCustomer.TradingChain,
                                                    MaterialGroup1 = "",
                                                    MaterialGroup2 = "",
                                                    MaterialGroup3 = "",
                                                    PromotionId = "",
                                                    SkpNumber = "",
                                                    SkpAmount = "",
                                                    ConditionCurrency = "",
                                                    Rate = "",
                                                    Per = "",
                                                    UnitOfMeasure = "",
                                                    ValidFrom = top1ListRulesByCustomer.PeriodStart,
                                                    ValidTo = top1ListRulesByCustomer.PeriodEnd,
                                                    AccrueDollar = "",
                                                    RebateBasis = "",
                                                    PayoutBasis = "",
                                                    PYSalValue = "",
                                                    PYAgreement = "",
                                                    RefCurrency = "",
                                                    OutletNumber = customer
                                                }
                                            );
                                        }

                                        foreach (var rule in listRulesByCustomer)
                                        {
                                            if (rule.ProgramCat.ToLower() == "z002")
                                            {
                                                listRulesOfSellOut.Add(
                                                new RulesTemplate
                                                {
                                                    ConditionType = DEFAULT_VALUE.ConditionTypeSO,
                                                    Table = DEFAULT_VALUE.TableSO,
                                                    TradeName = "",
                                                    OperationalRegion = "",
                                                    SoldToParty = "",
                                                    MaterialGroup1 = rule.MaterialGroup1,
                                                    MaterialGroup2 = "",
                                                    MaterialGroup3 = "",
                                                    PromotionId = DEFAULT_VALUE.PromotionId,
                                                    SkpNumber = "",
                                                    SkpAmount = "",
                                                    ConditionCurrency = DEFAULT_VALUE.ConditionCurrency,
                                                    Rate = rule.RateIDR,
                                                    Per = DEFAULT_VALUE.Per,
                                                    UnitOfMeasure = DEFAULT_VALUE.UnitOfMeasure,
                                                    ValidFrom = rule.PeriodStart,
                                                    ValidTo = rule.PeriodEnd,
                                                    AccrueDollar = "",
                                                    RebateBasis = "",
                                                    PayoutBasis = "",
                                                    PYSalValue = "",
                                                    PYAgreement = "",
                                                    RefCurrency = "",
                                                    OutletNumber = customer
                                                });
                                            }

                                        }
                                        #endregion

                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }

                            foreach (var filter in listFilterHeaderMTTT)
                            {
                                var listDataByTradingChainAndOutletNumber = listUploadTemplate.Where(x => x.TradingChain.Equals(filter.TradingChain) && x.OutletNumber.Equals(filter.OutletNumber)).ToList();
                                if (listDataByTradingChainAndOutletNumber[0].MTGT.ToLower() == Trade.GT.ToString().ToLower())
                                {
                                    break;
                                }
                                else
                                {
                                    //Customer MT
                                    //Customer MT - TT
                                    #region Customer MT - TT
                                    if (listDataByTradingChainAndOutletNumber[0].Type.Substring(0, 2).ToLower() == TradeType.TT.ToString().ToLower())
                                    {
                                        #region Customer MT - TT
                                        //insert data ke variabel grouped by customer
                                        string masterRequestType = DEFAULT_VALUE.MasterRequestType;
                                        string tempDescription = listDataByTradingChainAndOutletNumber[0].CustGroupName;
                                        int lengthTempDescription = tempDescription.Length > DEFAULT_VALUE.MaxLengthDescription ? DEFAULT_VALUE.MaxLengthDescription : tempDescription.Length;
                                        string description = tempDescription.Substring(0, lengthTempDescription);
                                        string currency = DEFAULT_VALUE.CurrencyH;
                                        string validFrom = listDataByTradingChainAndOutletNumber[0].PeriodStart;
                                        string validTo = listDataByTradingChainAndOutletNumber[0].PeriodEnd;
                                        string salesOrganization = DEFAULT_VALUE.SalesOrganization;
                                        string distributionChannel = DEFAULT_VALUE.DistributionChannel;
                                        string division = DEFAULT_VALUE.Division;
                                        string contractValidFrom = listDataByTradingChainAndOutletNumber[0].PeriodStart;
                                        string contractValidTo = listDataByTradingChainAndOutletNumber[0].PeriodEnd;
                                        string marketChannel = listDataByTradingChainAndOutletNumber[0].MTGT;
                                        string tempType2 = listDataByTradingChainAndOutletNumber[0].Type;
                                        int lengthTempTempType = tempType2.Length > DEFAULT_VALUE.MaxLengthType ? DEFAULT_VALUE.MaxLengthType : tempType2.Length;
                                        string type2 = listDataByTradingChainAndOutletNumber[0].Type.Substring(0, lengthTempTempType);
                                        string externalDescription = DEFAULT_VALUE.ExternalDescription;
                                        string spendAmmountCurrency = DEFAULT_VALUE.SpendAmountCurrencyMTTP;
                                        string tempPaymentTerms = listDataByTradingChainAndOutletNumber[0].PaymentTerms;
                                        int lengthTempPaymentTerms = tempType2.Length > DEFAULT_VALUE.MaxLengthPaymentTerms ? DEFAULT_VALUE.MaxLengthPaymentTerms : tempType2.Length;
                                        string paymentTerms2 = listDataByTradingChainAndOutletNumber[0].PaymentTerms.Substring(0, lengthTempPaymentTerms);
                                        string payer = listDataByTradingChainAndOutletNumber[0].OutletNumber;
                                        string contactPerson = DEFAULT_VALUE.ContactPerson;
                                        string gmId = listDataByTradingChainAndOutletNumber[0].GMID;
                                        var headerTemplate = new HeaderTemplate
                                        {
                                            MasterRequestType = masterRequestType,
                                            Description = description,
                                            Currency = currency,
                                            ValidFrom = validFrom,
                                            ValidTo = validTo,
                                            SalesOrganization = salesOrganization,
                                            DistributionChannel = distributionChannel,
                                            Division = division,
                                            ContractValidFrom = contractValidFrom,
                                            ContractValidTo = contractValidTo,
                                            MarketChannel = marketChannel,
                                            Type = type2,
                                            ExternalDescription = externalDescription,
                                            SpendAmmountCurrency = spendAmmountCurrency,
                                            PaymentTerms = paymentTerms2,
                                            Payer = payer,
                                            ContactPerson = contactPerson,
                                            GMID = gmId,
                                            TradingChain = filter.TradingChain
                                        };
                                        listHeaderMTTT.Add(headerTemplate);

                                        //count customer MT-TT
                                        resultCount++;

                                        var listProgramCategoryByTradingChain = listUploadTemplate.Where(x => x.TradingChain.Equals(filter.TradingChain) && x.OutletNumber.Equals(filter.OutletNumber)).Distinct(new ProgramCatComparer());
                                        foreach (var programCategory in listProgramCategoryByTradingChain)
                                        {
                                            listProgramCategoryMTTT.Add(
                                                new ProgramCategoryTemplate
                                                {
                                                    PromotionID = programCategory.ProgramCat,
                                                    SettlementFactor = DEFAULT_VALUE.SettlementFactor,
                                                    SettlementMethod = programCategory.Settlement.Substring(0, 2),
                                                    SpendAmount = programCategory.BudgetPerProgramCat,
                                                    SettlementFrequency = DEFAULT_VALUE.SettlementFrequency,
                                                    PaymentLevel = DEFAULT_VALUE.PaymentLevel,
                                                    RebateReceipnt = programCategory.OutletNumber,
                                                    ValidFrom = programCategory.PeriodStart,
                                                    ValidTo = programCategory.PeriodEnd,
                                                    ProgramCategory = programCategory.ProgramCat,
                                                    Currency = DEFAULT_VALUE.CurrencyPC,
                                                    TradingChain = filter.TradingChain
                                                }
                                            );
                                        }

                                        var listRulesByTradingChain = listUploadTemplate.Where(x => x.TradingChain.Equals(filter.TradingChain) && x.OutletNumber.Equals(filter.OutletNumber));
                                        foreach (var rule in listRulesByTradingChain)
                                        {
                                            if (rule.ProgramCat.ToLower() != DEFAULT_VALUE.ExcludeProgramCat.ToLower())
                                            {
                                                listRulesOfProgramCategoryMTTT.Add(
                                                    new RulesMTTemplate
                                                    {
                                                        ConditionType = rule.MaterialGroup1 == "" ? DEFAULT_VALUE.ConditionTypeEmpty : DEFAULT_VALUE.ConditionTypeNotEmpty,
                                                        Table = DEFAULT_VALUE.Table,
                                                        TradeName = "",
                                                        OperationalRegion = "",
                                                        SoldToParty = "",
                                                        MaterialGroup1 = rule.MaterialGroup1,
                                                        MaterialGroup2 = "",
                                                        MaterialGroup3 = "",
                                                        PromotionId = rule.ProgramCat,
                                                        SkpNumber = "",
                                                        SkpAmount = "",
                                                        ConditionCurrency = rule.RatePercentage != "" ? DEFAULT_VALUE.ConditionCurrencyPC : "",
                                                        Rate = rule.RatePercentage,
                                                        Per = "",
                                                        UnitOfMeasure = "",
                                                        ValidFrom = rule.PeriodStart,
                                                        ValidTo = rule.PeriodEnd,
                                                        AccrueDollar = rule.RateIDR,
                                                        RebateBasis = "",
                                                        PayoutBasis = "",
                                                        PYSalValue = "",
                                                        PYAgreement = "",
                                                        RefCurrency = "",
                                                        TradingChain = filter.TradingChain,
                                                        OutletNumber = filter.OutletNumber
                                                    }
                                                );

                                            }
                                        }

                                        listRulesOfEligibleTradingChainMTTT.Add(
                                            new RulesMTTemplate
                                            {
                                                ConditionType = "ZV01",
                                                Table = "750",
                                                TradeName = listDataByTradingChainAndOutletNumber[0].TradingChain,
                                                OperationalRegion = "",
                                                SoldToParty = "",
                                                MaterialGroup1 = "",
                                                MaterialGroup2 = "",
                                                MaterialGroup3 = "",
                                                PromotionId = "",
                                                SkpNumber = "",
                                                SkpAmount = "",
                                                ConditionCurrency = "",
                                                Rate = "",
                                                Per = "",
                                                UnitOfMeasure = "",
                                                ValidFrom = listDataByTradingChainAndOutletNumber[0].PeriodStart,
                                                ValidTo = listDataByTradingChainAndOutletNumber[0].PeriodEnd,
                                                AccrueDollar = listDataByTradingChainAndOutletNumber[0].RateIDR,
                                                RebateBasis = "",
                                                PayoutBasis = "",
                                                PYSalValue = "",
                                                PYAgreement = "",
                                                RefCurrency = "",
                                                TradingChain = filter.TradingChain,
                                                OutletNumber = filter.OutletNumber
                                            }
                                        );



                                        var isGrowthEstmAccrualExist = listDataByTradingChainAndOutletNumber.Where(x => x.ProgramCat.ToString().Equals("Z016")).Any();



                                        if (isGrowthEstmAccrualExist)
                                        {

                                            listRulesOfGrowthAllProductMTTT.Add(
                                            new RulesMTTemplate
                                            {
                                                ConditionType = "ZV28",
                                                Table = "751",
                                                TradeName = "",
                                                OperationalRegion = "",
                                                SoldToParty = "",
                                                MaterialGroup1 = "",
                                                MaterialGroup2 = "",
                                                MaterialGroup3 = "",
                                                PromotionId = "Z016",
                                                SkpNumber = "",
                                                SkpAmount = "",
                                                ConditionCurrency = "",
                                                Rate = "",
                                                Per = "",
                                                UnitOfMeasure = "",
                                                ValidFrom = listDataByTradingChainAndOutletNumber[0].PeriodStart,
                                                ValidTo = listDataByTradingChainAndOutletNumber[0].PeriodEnd,
                                                AccrueDollar = listDataByTradingChainAndOutletNumber[0].RateIDR,
                                                RebateBasis = "",
                                                PayoutBasis = "",
                                                PYSalValue = "",
                                                PYAgreement = "",
                                                RefCurrency = "",
                                                TradingChain = filter.TradingChain,
                                                OutletNumber = filter.OutletNumber
                                            }
                                        );

                                            listRulesOfGrowthEstmAccrualMTTT.Add(
                                                new RulesMTTemplate
                                                {
                                                    ConditionType = "ZV35",
                                                    Table = "754",
                                                    TradeName = "",
                                                    OperationalRegion = "",
                                                    SoldToParty = "",
                                                    MaterialGroup1 = "",
                                                    MaterialGroup2 = "",
                                                    MaterialGroup3 = "",
                                                    PromotionId = "Z016",
                                                    SkpNumber = "",
                                                    SkpAmount = "",
                                                    ConditionCurrency = "%",
                                                    Rate = listDataByTradingChainAndOutletNumber[0].RatePercentage,
                                                    Per = "",
                                                    UnitOfMeasure = "",
                                                    ValidFrom = listDataByTradingChainAndOutletNumber[0].PeriodStart,
                                                    ValidTo = listDataByTradingChainAndOutletNumber[0].PeriodEnd,
                                                    AccrueDollar = "",
                                                    RebateBasis = "",
                                                    PayoutBasis = "",
                                                    PYSalValue = "",
                                                    PYAgreement = "",
                                                    RefCurrency = "",
                                                    TradingChain = filter.TradingChain,
                                                    OutletNumber = filter.OutletNumber
                                                }
                                            );

                                            listRulesOfGrowthCalculationMTTT.Add(
                                                new RulesMTTemplate
                                                {
                                                    ConditionType = "ZV40",
                                                    Table = "754",
                                                    TradeName = "",
                                                    OperationalRegion = "",
                                                    SoldToParty = "",
                                                    MaterialGroup1 = "",
                                                    MaterialGroup2 = "",
                                                    MaterialGroup3 = "",
                                                    PromotionId = "Z016",
                                                    SkpNumber = "",
                                                    SkpAmount = "",
                                                    ConditionCurrency = "",
                                                    Rate = "",
                                                    Per = "",
                                                    UnitOfMeasure = "",
                                                    ValidFrom = listDataByTradingChainAndOutletNumber[0].PeriodStart,
                                                    ValidTo = listDataByTradingChainAndOutletNumber[0].PeriodEnd,
                                                    AccrueDollar = "",
                                                    RebateBasis = "3",
                                                    PayoutBasis = "1",
                                                    PYSalValue = listDataByTradingChainAndOutletNumber[0].PreviousYearSalesValue,
                                                    PYAgreement = "",
                                                    RefCurrency = "",
                                                    TradingChain = filter.TradingChain,
                                                    OutletNumber = filter.OutletNumber
                                                }
                                            );


                                            listRulesOfGrowthTierMTTT.Add(
                                                new RulesMTTemplate
                                                {
                                                    ConditionType = "ZV38",
                                                    Table = "754",
                                                    TradeName = "",
                                                    OperationalRegion = "",
                                                    SoldToParty = "",
                                                    MaterialGroup1 = "",
                                                    MaterialGroup2 = "",
                                                    MaterialGroup3 = "",
                                                    PromotionId = "Z016",
                                                    SkpNumber = "",
                                                    SkpAmount = "",
                                                    ConditionCurrency = "%",
                                                    Rate = listDataByTradingChainAndOutletNumber[0].RatePercentage,
                                                    Per = "",
                                                    UnitOfMeasure = "",
                                                    ValidFrom = listDataByTradingChainAndOutletNumber[0].PeriodStart,
                                                    ValidTo = listDataByTradingChainAndOutletNumber[0].PeriodEnd,
                                                    AccrueDollar = "",
                                                    RebateBasis = "",
                                                    PayoutBasis = "",
                                                    PYSalValue = "",
                                                    PYAgreement = "",
                                                    RefCurrency = "",
                                                    TradingChain = filter.TradingChain,
                                                    OutletNumber = filter.OutletNumber
                                                }
                                            );

                                            var listGrowthEstmAccrualExist = listDataByTradingChainAndOutletNumber.Where(x => x.ProgramCat.ToString().Equals("Z016")).ToList();

                                            foreach (var item in listGrowthEstmAccrualExist)
                                            {
                                                if (item.GrowthScal1GrowthPercentage != "")
                                                {
                                                    var growthScale1 = new GrowthScale
                                                    {
                                                        GrowthPercentage = item.GrowthScal1GrowthPercentage,
                                                        ScaleCurrency = "IDR",
                                                        ScaleQuantity = "",
                                                        ScaleUnitOfMeasure = "",
                                                        ConditionCurrency = "%",
                                                        ConditionRate = item.GrowthScal1RatePercentage,
                                                        TradingChain = filter.TradingChain,
                                                        OutletNumber = filter.OutletNumber
                                                    };
                                                    listRulesOfGrowthScaleMTTT.Add(growthScale1);
                                                }

                                                if (item.GrowthScal2GrowthPercentage != "")
                                                {
                                                    var growthScale2 = new GrowthScale
                                                    {
                                                        GrowthPercentage = item.GrowthScal2GrowthPercentage,
                                                        ScaleCurrency = "IDR",
                                                        ScaleQuantity = "",
                                                        ScaleUnitOfMeasure = "",
                                                        ConditionCurrency = "%",
                                                        ConditionRate = item.GrowthScal2RatePercentage,
                                                        TradingChain = filter.TradingChain,
                                                        OutletNumber = filter.OutletNumber
                                                    };
                                                    listRulesOfGrowthScaleMTTT.Add(growthScale2);
                                                }

                                                if (item.GrowthScal3GrowthPercentage != "")
                                                {
                                                    var growthScale3 = new GrowthScale
                                                    {
                                                        GrowthPercentage = item.GrowthScal3GrowthPercentage,
                                                        ScaleCurrency = "IDR",
                                                        ScaleQuantity = "",
                                                        ScaleUnitOfMeasure = "",
                                                        ConditionCurrency = "%",
                                                        ConditionRate = item.GrowthScal3RatePercentage,
                                                        TradingChain = filter.TradingChain,
                                                        OutletNumber = filter.OutletNumber
                                                    };

                                                    listRulesOfGrowthScaleMTTT.Add(growthScale3);
                                                }
                                            }


                                        }
                                        #endregion
                                    }
                                    #endregion
                                }
                            }
                        }
                        else
                        {
                            int currentIndex = 1;

                            if (agreementType == "overAndAbove")
                            {
                                //Customer MT - TP Auto
                                #region Customer MT - TP auto
                                foreach (var data in listUploadTemplate)
                                {

                                    string masterRequestType = DEFAULT_VALUE.MasterRequestType;
                                    string tempDescription = data.CustGroupName;
                                    int lengthTempDescription = tempDescription.Length > DEFAULT_VALUE.MaxLengthDescription ? DEFAULT_VALUE.MaxLengthDescription : tempDescription.Length;
                                    string description = tempDescription.Substring(0, lengthTempDescription);
                                    string currency = DEFAULT_VALUE.CurrencyH;
                                    string validFrom = data.PeriodStart;
                                    string validTo = data.PeriodEnd;
                                    string salesOrganization = DEFAULT_VALUE.SalesOrganization;
                                    string distributionChannel = DEFAULT_VALUE.DistributionChannel;
                                    string division = DEFAULT_VALUE.Division;
                                    string contractValidFrom = data.PeriodStart;
                                    string contractValidTo = data.PeriodEnd;
                                    string marketChannel = data.MTGT;
                                    string tempType2 = data.Type;
                                    int lengthTempTempType = tempType2.Length > DEFAULT_VALUE.MaxLengthType ? DEFAULT_VALUE.MaxLengthType : tempType2.Length;
                                    string type2 = data.Type.Substring(0, lengthTempTempType);
                                    string externalDescription = "Over and Above";
                                    string spendAmmountCurrency = DEFAULT_VALUE.SpendAmountCurrencyMTTP;
                                    string tempPaymentTerms = data.PaymentTerms;
                                    int lengthTempPaymentTerms = tempType2.Length > DEFAULT_VALUE.MaxLengthPaymentTerms ? DEFAULT_VALUE.MaxLengthPaymentTerms : tempType2.Length;
                                    string paymentTerms2 = data.PaymentTerms.Substring(0, lengthTempPaymentTerms);
                                    string payer = data.OutletNumber;
                                    string contactPerson = DEFAULT_VALUE.ContactPerson;
                                    string gmId = data.GMID;

                                    listHeaderMTTPAuto.Add(new HeaderTemplate
                                    {
                                        MasterRequestType = masterRequestType,
                                        Description = description,
                                        ExternalDescription = externalDescription,
                                        ValidFrom = validFrom,
                                        ValidTo = validTo,
                                        ContractValidFrom = contractValidFrom,
                                        ContractValidTo = contractValidTo,
                                        MarketChannel = marketChannel,
                                        Type = type2,
                                        PaymentTerms = paymentTerms2,
                                        Payer = payer,
                                        ContactPerson = contactPerson,
                                        StatusFlowApprover = gmId,
                                        Index = currentIndex
                                    });
                                    //count customer MT-TP
                                    resultCount++;
                                    listProgramCategoryMTTPAuto.Add(new ProgramCategoryTemplate
                                    {
                                        ProgramCategory = data.ProgramCat,
                                        SettlementFactor = DEFAULT_VALUE.SettlementFactor,
                                        SettlementMethod = data.Settlement.Substring(0, 2),
                                        SpendAmount = data.BudgetPerProgramCat,
                                        RebateReceipnt = data.OutletNumber,
                                        ValidFrom = data.PeriodStart,
                                        ValidTo = data.PeriodEnd,
                                        Index = currentIndex
                                    });

                                    listRulesOfProgramCategoryMTTPAuto.Add(new RulesMTTemplate
                                    {

                                        PriceSheetDescription = "BENEFIT - MG1 BY PROGRAM",
                                        Table = "",
                                        TradeName = "",
                                        OperationalRegion = "",
                                        PromotionId = "Z023",
                                        MaterialGroup1 = data.MaterialGroup1,
                                        AccrueDollar = data.RateIDR,
                                        DisDollarUOM = "",
                                        ValidFrom = data.PeriodStart,
                                        ValidTo = data.PeriodEnd,
                                        Index = currentIndex
                                    });

                                    if (data.EligibleOperationalRegion != "")
                                    {
                                        listRulesOfEligiblityOperationalRegionMTTPAuto.Add(new RulesMTTemplate
                                        {
                                            PriceSheetDescription = "ELIGIBILITY- OPERATIONAL REGION",
                                            Table = "",
                                            TradeName = data.TradingChain,
                                            OperationalRegion = data.EligibleOperationalRegion == "" ? "" : data.EligibleOperationalRegion.Substring(0, 1),
                                            PromotionID = "",
                                            MaterialGroup1 = "",
                                            AccrueDollar = "",
                                            DisDollarUOM = "",
                                            ValidFrom = data.PeriodStart,
                                            ValidTo = data.PeriodEnd,
                                            Index = currentIndex
                                        });
                                    }
                                    else
                                    {
                                        listRulesOfEligiblityTradingChainMTTPAuto.Add(new RulesMTTemplate
                                        {
                                            PriceSheetDescription = "ELIGIBILITY- TRADING CHAIN",
                                            Table = "",
                                            TradeName = data.TradingChain,
                                            OperationalRegion = "",
                                            PromotionID = "",
                                            MaterialGroup1 = "",
                                            AccrueDollar = "",
                                            DisDollarUOM = "",
                                            ValidFrom = data.PeriodStart,
                                            ValidTo = data.PeriodEnd,
                                            Index = currentIndex
                                        });
                                    }

                                    currentIndex++;
                                }
                                #endregion
                            }
                            else
                            {
                                //Customer MT - TP
                                #region Customer MT - TP
                                foreach (var data in listUploadTemplate)
                                {

                                    string masterRequestType = DEFAULT_VALUE.MasterRequestType;
                                    string tempDescription = data.CustGroupName;
                                    int lengthTempDescription = tempDescription.Length > DEFAULT_VALUE.MaxLengthDescription ? DEFAULT_VALUE.MaxLengthDescription : tempDescription.Length;
                                    string description = tempDescription.Substring(0, lengthTempDescription);
                                    string currency = DEFAULT_VALUE.CurrencyH;
                                    string validFrom = data.PeriodStart;
                                    string validTo = data.PeriodEnd;
                                    string salesOrganization = DEFAULT_VALUE.SalesOrganization;
                                    string distributionChannel = DEFAULT_VALUE.DistributionChannel;
                                    string division = DEFAULT_VALUE.Division;
                                    string contractValidFrom = data.PeriodStart;
                                    string contractValidTo = data.PeriodEnd;
                                    string marketChannel = data.MTGT;
                                    string tempType2 = data.Type;
                                    int lengthTempTempType = tempType2.Length > DEFAULT_VALUE.MaxLengthType ? DEFAULT_VALUE.MaxLengthType : tempType2.Length;
                                    string type2 = data.Type.Substring(0, lengthTempTempType);
                                    string externalDescription = DEFAULT_VALUE.ExternalDescription;
                                    string spendAmmountCurrency = DEFAULT_VALUE.SpendAmountCurrencyMTTP;
                                    string tempPaymentTerms = data.PaymentTerms;
                                    int lengthTempPaymentTerms = tempType2.Length > DEFAULT_VALUE.MaxLengthPaymentTerms ? DEFAULT_VALUE.MaxLengthPaymentTerms : tempType2.Length;
                                    string paymentTerms2 = data.PaymentTerms.Substring(0, lengthTempPaymentTerms);
                                    string payer = data.OutletNumber;
                                    string contactPerson = DEFAULT_VALUE.ContactPerson;
                                    string gmId = data.GMID;

                                    listHeaderMTTP.Add(new HeaderTemplate
                                    {
                                        MasterRequestType = masterRequestType,
                                        Description = description,
                                        Currency = currency,
                                        ValidFrom = validFrom,
                                        ValidTo = validTo,
                                        SalesOrganization = salesOrganization,
                                        DistributionChannel = distributionChannel,
                                        Division = division,
                                        ContractValidFrom = contractValidFrom,
                                        ContractValidTo = contractValidTo,
                                        MarketChannel = marketChannel,
                                        Type = type2,
                                        ExternalDescription = externalDescription,
                                        SpendAmmountCurrency = spendAmmountCurrency,
                                        PaymentTerms = paymentTerms2,
                                        Payer = payer,
                                        ContactPerson = contactPerson,
                                        GMID = gmId,
                                        Index = currentIndex
                                    });
                                    //count customer MT-TP
                                    resultCount++;
                                    listProgramCategoryMTTP.Add(new ProgramCategoryTemplate
                                    {
                                        PromotionID = data.ProgramCat,
                                        SettlementFactor = DEFAULT_VALUE.SettlementFactor,
                                        SettlementMethod = data.Settlement.Substring(0, 2),
                                        SpendAmount = data.BudgetPerProgramCat,
                                        SettlementFrequency = DEFAULT_VALUE.SettlementFrequency,
                                        PaymentLevel = DEFAULT_VALUE.PaymentLevel,
                                        RebateReceipnt = data.OutletNumber,
                                        ValidFrom = data.PeriodStart,
                                        ValidTo = data.PeriodEnd,
                                        ProgramCategory = data.ProgramCat,
                                        Currency = DEFAULT_VALUE.CurrencyPC,
                                        Index = currentIndex
                                    });

                                    listRulesOfProgramCategoryMTTP.Add(new RulesMTTemplate
                                    {
                                        ConditionType = "ZV11",
                                        Table = "751",
                                        TradeName = "",
                                        OperationalRegion = "",
                                        SoldToParty = "",
                                        MaterialGroup1 = data.MaterialGroup1,
                                        MaterialGroup2 = "",
                                        MaterialGroup3 = "",
                                        PromotionId = "Z023",
                                        SkpNumber = "",
                                        SkpAmount = "",
                                        ConditionCurrency = "",
                                        Rate = "",
                                        Per = "",
                                        UnitOfMeasure = "",
                                        ValidFrom = data.PeriodStart,
                                        ValidTo = data.PeriodEnd,
                                        AccrueDollar = data.RateIDR,
                                        DisDollarUOM = "",
                                        RebateBasis = "",
                                        PayoutBasis = "",
                                        PYSalValue = "",
                                        PYAgreement = "",
                                        RefCurrency = "",
                                        Index = currentIndex
                                    });


                                    listRulesOfEligibleChainOrTradingChainOperationalRegion.Add(new RulesMTTemplate
                                    {
                                        ConditionType = data.EligibleOperationalRegion == "" ? "ZV01" : "ZV02",
                                        Table = "750",
                                        TradeName = data.TradingChain,
                                        OperationalRegion = data.EligibleOperationalRegion == "" ? "" : data.EligibleOperationalRegion.Substring(0, 1),
                                        SoldToParty = "",
                                        MaterialGroup1 = "",
                                        MaterialGroup2 = "",
                                        MaterialGroup3 = "",
                                        PromotionId = "",
                                        SkpNumber = "",
                                        SkpAmount = "",
                                        ConditionCurrency = "",
                                        Rate = "",
                                        Per = "",
                                        UnitOfMeasure = "",
                                        ValidFrom = data.PeriodStart,
                                        ValidTo = data.PeriodEnd,
                                        AccrueDollar = "",
                                        DisDollarUOM = "",
                                        RebateBasis = "",
                                        PayoutBasis = "",
                                        PYSalValue = "",
                                        PYAgreement = "",
                                        RefCurrency = "",
                                        Index = currentIndex
                                    });

                                    currentIndex++;
                                }
                                #endregion
                            }

                        }

                        var excelGt = new ExportExcelTemplateGT
                        {
                            ListHeaderTemplate = listHeader,
                            ListProgramCategoryTemplate = listProgramCategory,
                            ListRulesOfProgramCategory = listRulesOfProgramCategory,
                            ListRulesOfEligibleSoldTo = listRulesOfEligibleSoldTo,
                            ListRulesOfSellOut = listRulesOfSellOut
                        };

                        var excelMtTT = new ExportExcelTemplateMTTT
                        {
                            ListHeaderTemplate = listHeaderMTTT,
                            ListProgramCategoryTemplate = listProgramCategoryMTTT,
                            ListRulesOfProgramCategory = listRulesOfProgramCategoryMTTT,
                            ListRulesOfEligibleTradingChain = listRulesOfEligibleTradingChainMTTT,
                            ListRulesOfGrowthAllProduct = listRulesOfGrowthAllProductMTTT,
                            ListRulesOfGrowthEstmAccrual = listRulesOfGrowthEstmAccrualMTTT,
                            ListRulesOfGrowthCalculation = listRulesOfGrowthCalculationMTTT,
                            ListRulesOfGrowthScale = listRulesOfGrowthScaleMTTT,
                            ListRulesOfGrowthTier = listRulesOfGrowthTierMTTT
                        };

                        var excelMtTP = new ExportExcelTemplateMTTP
                        {
                            ListHeaderTemplate = listHeaderMTTP,
                            ListProgramCategoryTemplate = listProgramCategoryMTTP,
                            ListRulesOfProgramCategory = listRulesOfProgramCategoryMTTP,
                            ListRulesOfEligibleChainOrOperationalRegion = listRulesOfEligibleChainOrTradingChainOperationalRegion,
                        };
                        var excelMtTPAuto = new ExportExcelTemplateMTTPAuto
                        {
                            ListHeaderTemplate = listHeaderMTTPAuto,
                            ListProgramCategoryTemplate = listProgramCategoryMTTPAuto,
                            ListRulesOfProgramCategory = listRulesOfProgramCategoryMTTPAuto,
                            ListRulesOfEligibilityOperationalRegion = listRulesOfEligiblityOperationalRegionMTTPAuto,
                            ListRulesOfEligibilityTradingChain = listRulesOfEligiblityTradingChainMTTPAuto
                        };
                        return new ExportExcelWrapper
                        {
                            Trade = trade,
                            TradeType = tradeType,
                            ExcelTemplateForGT = excelGt,
                            ExcelTemplateForMTTT = excelMtTT,
                            ExcelTemplateForMTTP = excelMtTP,
                            ExcelTemplateForMTTPAuto = excelMtTPAuto,
                            IsSucced = true,
                            ErrorLog = listErrorLog,
                            ResultCount = resultCount
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                listErrorLog.Add(ex.ToString());
                return new ExportExcelWrapper
                {
                    IsSucced = false,
                    ErrorLog = listErrorLog

                };
            }
            return new ExportExcelWrapper
            {
                IsSucced = false,
                ErrorLog = listErrorLog

            };
        }

        public void AddData(Conversion model)
        {
            var DEFAULT_VALUE = new DefaultValue();
            string dbPath = Server.MapPath(DEFAULT_VALUE.DbPath);
            using (var db = new LiteDatabase(dbPath))
            {
                var conversions = db.GetCollection<Conversion>("conversions");
                conversions.Insert(model);
            }
        }

        public string GetData(string guidId)
        {
            var DEFAULT_VALUE = new DefaultValue();
            string dbPath = Server.MapPath(DEFAULT_VALUE.DbPath);
            using (var db = new LiteDatabase(dbPath))
            {
                var conversions = db.GetCollection<Conversion>("conversions");

                var result = conversions.FindOne(x => x.Id.Equals(guidId));


                if (result != null)
                {
                    return result.Value;
                }
                else
                    return null;
            }
        }

        public void DeleteData(string guidId)
        {
            var DEFAULT_VALUE = new DefaultValue();
            string dbPath = Server.MapPath(DEFAULT_VALUE.DbPath);
            using (var db = new LiteDatabase(dbPath))
            {
                var conversions = db.GetCollection<Conversion>("conversions");
                conversions.Delete(x => x.Id.Equals(guidId));
                db.Shrink();
            }
        }

        public void DeleteOldData()
        {
            var DEFAULT_VALUE = new DefaultValue();
            string dbPath = Server.MapPath(DEFAULT_VALUE.DbPath);
            using (var db = new LiteDatabase(dbPath))
            {
                var conversions = db.GetCollection<Conversion>("conversions");
                var oldTime = DateTime.Now.AddDays(-1);
                conversions.Delete(x => x.CreatedDate <= oldTime);
                db.Shrink();
            }
        }

        public void CheckAllData()
        {
            var DEFAULT_VALUE = new DefaultValue();
            string dbPath = Server.MapPath(DEFAULT_VALUE.DbPath);
            using (var db = new LiteDatabase(dbPath))
            {
                var conversions = db.GetCollection<Conversion>("conversions");

                var result = conversions.FindAll();
            }
        }

    }
}